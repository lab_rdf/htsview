/**
 * Copyright 2017 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.htsview.tracks.loaders;

import java.io.IOException;
import java.nio.file.Path;

import org.abh.common.json.Json;
import org.abh.common.tree.TreeNode;
import org.abh.common.ui.window.ModernWindow;
import org.abh.sequencing.tracks.Track;
import org.abh.sequencing.tracks.sample.SampleAssemblyBRT;

import edu.columbia.rdf.edb.Sample;

// TODO: Auto-generated Javadoc
/**
 * The Class SampleLoaderBRT.
 */
public class SampleLoaderBRT extends SampleLoaderBin {
	
	/* (non-Javadoc)
	 * @see org.abh.common.NameProperty#getName()
	 */
	@Override
	public String getName() {
		return "BRT";
	}

	/* (non-Javadoc)
	 * @see org.abh.sequencing.tracks.loaders.SampleLoader#getExt()
	 */
	@Override
	public String getExt() {
		return "brtj";
	}
	
	/* (non-Javadoc)
	 * @see org.htsview.tracks.loaders.SampleLoaderBin#openSample(org.abh.common.ui.window.ModernWindow, edu.columbia.rdf.edb.Sample, java.nio.file.Path, org.abh.common.json.Json, org.abh.common.tree.TreeNode)
	 */
	@Override
	public Track openSample(ModernWindow parent,
			Sample sample,
			Path metaFile,
			Json json,
			TreeNode<Track> root) throws IOException {
		return openSampleFs(sample, 
				new SampleAssemblyBRT(metaFile), 
				metaFile,
				root);
	}

	/* (non-Javadoc)
	 * @see org.htsview.tracks.loaders.SampleLoaderBin#openReads(org.abh.common.ui.window.ModernWindow, edu.columbia.rdf.edb.Sample, java.nio.file.Path, org.abh.common.json.Json, org.abh.common.tree.TreeNode)
	 */
	@Override
	public Track openReads(ModernWindow parent,
			Sample sample,
			Path metaFile,
			Json json,
			TreeNode<Track> root) throws IOException {
		return openReadsFs(sample, 
				new SampleAssemblyBRT(metaFile),
				metaFile,
				root);
	}
}
