/**
 * Copyright 2017 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.htsview.tracks.loaders;

import java.io.IOException;
import java.nio.file.Path;

import org.abh.common.io.FileUtils;
import org.abh.common.json.Json;
import org.abh.common.json.JsonParser;
import org.abh.common.tree.TreeNode;
import org.abh.common.ui.window.ModernWindow;
import org.abh.sequencing.tracks.Track;
import org.abh.sequencing.tracks.loaders.SampleLoaderFS;
import org.abh.sequencing.tracks.sample.SampleAssemblyBVT;
import org.abh.sequencing.tracks.sample.SampleTracks;

import edu.columbia.rdf.edb.Sample;

// TODO: Auto-generated Javadoc
/**
 * The Class SampleLoaderBVT.
 */
public class SampleLoaderBVT extends SampleLoaderFS {

	/* (non-Javadoc)
	 * @see org.abh.sequencing.tracks.loaders.SampleLoader#openSample(org.abh.common.ui.window.ModernWindow, java.nio.file.Path, org.abh.common.tree.TreeNode)
	 */
	@Override
	public Track openSample(ModernWindow parent,
			Path metaFile, 
			TreeNode<Track> root) throws IOException {
		if (!FileUtils.exists(metaFile)) {
			return null;
		}

		Json json = JsonParser.json(metaFile);

		Sample sample = SampleTracks.getSampleFromTrack(json);

		return openSampleFs(sample, new SampleAssemblyBVT(metaFile), metaFile, root);
	}
	
	/* (non-Javadoc)
	 * @see org.abh.common.NameProperty#getName()
	 */
	@Override
	public String getName() {
		return "BVT";
	}

	/* (non-Javadoc)
	 * @see org.abh.sequencing.tracks.loaders.SampleLoader#getExt()
	 */
	@Override
	public String getExt() {
		return "bvtj";
	}
}
