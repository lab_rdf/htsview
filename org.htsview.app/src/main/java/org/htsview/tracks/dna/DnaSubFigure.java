/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.htsview.tracks.dna;

import java.awt.Color;

import org.abh.common.bioinformatics.genomic.GenomicRegion;
import org.abh.sequencing.tracks.FixedSubFigure;
import org.abh.sequencing.tracks.Track;
import org.graphplot.figure.PlotStyle;

// TODO: Auto-generated Javadoc
/**
 * The Class DnaSubFigure.
 */
public abstract class DnaSubFigure extends FixedSubFigure {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/* (non-Javadoc)
	 * @see org.abh.sequencing.tracks.FixedSubFigure#update(org.abh.common.bioinformatics.genome.GenomicRegion, int, double, int, int, int, java.awt.Color, java.awt.Color, org.graphplot.figure.PlotStyle)
	 */
	@Override
	public void update(GenomicRegion displayRegion, 
			int resolution,
			double yMax,
			int width,
			int height,
			int margin,
			Color lineColor,
			Color fillColor,
			PlotStyle style) {
		
		super.update(displayRegion, 
				resolution,
				yMax,
				width, 
				Track.SMALL_TRACK_SIZE.height,
				margin,
				lineColor, 
				fillColor,
				style);
	}
}
