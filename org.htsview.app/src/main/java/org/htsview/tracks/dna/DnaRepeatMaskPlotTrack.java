/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.htsview.tracks.dna;

import java.io.IOException;

import org.abh.common.bioinformatics.genomic.GenomeAssembly;
import org.abh.common.bioinformatics.genomic.GenomicRegion;
import org.abh.sequencing.tracks.TitleProperties;
import org.abh.sequencing.tracks.TrackSubFigure;
import org.graphplot.figure.Axes2D;

// TODO: Auto-generated Javadoc
/**
 * The Class DnaRepeatMaskPlotTrack.
 */
public class DnaRepeatMaskPlotTrack extends DnaPlotTrack {
	
	/** The title. */
	public static String TITLE = "DNA Repeat Mask";
	
	/**
	 * Instantiates a new dna repeat mask plot track.
	 *
	 * @param genomeAssembly the genome assembly
	 */
	public DnaRepeatMaskPlotTrack(GenomeAssembly genomeAssembly) {
		super(TITLE, genomeAssembly);
	}

	/* (non-Javadoc)
	 * @see org.abh.sequencing.tracks.Track#createGraph(java.lang.String, org.abh.sequencing.tracks.TitleProperties)
	 */
	@Override
	public TrackSubFigure createGraph(String genome,
			TitleProperties titlePosition) throws IOException {
		
		//
		// Display some genes
		//
		
		mSubFigure = DnaRepeatMaskPlotCanvas.create(genome,
				mGenomeAssembly,
				titlePosition);
		
			
		switch(titlePosition.getPosition()) {
		case RIGHT:
		case COMPACT_RIGHT:	
			int right = rightTitleWidth(getName());
			mSubFigure.getCurrentAxes().setMargins(SMALL_MARGIN, MARGINS.getLeft(), SMALL_MARGIN, right);
			break;
		default:
			mSubFigure.getCurrentAxes().setMargins(MARGINS);
		}
		
		Axes2D.disableAllFeatures(mSubFigure.getCurrentAxes());
		
		return mSubFigure;
	}

	/* (non-Javadoc)
	 * @see org.abh.sequencing.tracks.Track#updateGraph(org.abh.common.bioinformatics.genome.GenomicRegion, int, int, int, int)
	 */
	@Override
	public TrackSubFigure updateGraph(GenomicRegion displayRegion, 
			int resolution,
			int width,
			int height,
			int margin) throws IOException {
		
		//mSubFigure.setForwardCanvasEventsEnabled(false);
		mSubFigure.update(displayRegion, resolution, width, height, margin);
		//mSubFigure.setForwardCanvasEventsEnabled(true);
		
		return mSubFigure;
	}
	
}
