/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.htsview.tracks.dna;

import java.awt.Graphics2D;
import java.io.IOException;

import org.abh.common.bioinformatics.genomic.Dna;
import org.abh.common.bioinformatics.genomic.GenomeAssembly;
import org.abh.common.bioinformatics.genomic.GenomicRegion;
import org.abh.common.bioinformatics.genomic.RepeatMaskType;
import org.abh.common.bioinformatics.genomic.SequenceRegion;
import org.abh.common.ui.graphics.DrawingContext;
import org.graphplot.figure.Axes;
import org.graphplot.figure.AxesClippedLayer;
import org.graphplot.figure.SubFigure;

// TODO: Auto-generated Javadoc
/**
 * The Class DnaColorCanvasLayer.
 */
public class DnaColorCanvasLayer extends AxesClippedLayer {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;


	
	/** The m display region. */
	private GenomicRegion mDisplayRegion;

	/** The m assembly. */
	private GenomeAssembly mAssembly;



	/** The m genome. */
	private String mGenome;

	/**
	 * Instantiates a new dna color canvas layer.
	 *
	 * @param genome the genome
	 * @param assembly the assembly
	 */
	public DnaColorCanvasLayer(String genome, GenomeAssembly assembly) {
		super("DNA Color");

		mGenome = genome;
		mAssembly = assembly;
	}
	
	/**
	 * Update.
	 *
	 * @param displayRegion the display region
	 */
	public void update(GenomicRegion displayRegion) {
		mDisplayRegion = displayRegion;
	}

	/* (non-Javadoc)
	 * @see org.graphplot.figure.AxesClippedLayer#plotClipped(java.awt.Graphics2D, org.abh.common.ui.graphics.DrawingContext, org.graphplot.figure.SubFigure, org.graphplot.figure.Axes)
	 */
	@Override
	public void plotClipped(Graphics2D g2,
			DrawingContext context,
			SubFigure figure,
			Axes axes) {
		
		// So that we don't attempt to pull a whole chromosome
		if (mDisplayRegion.getLength() > DnaPlotTrack.MAX_DISPLAY_BASES) {
			return;
		}


		int x1;
		int y = 0;
		int h = axes.getInternalPlotSize().getH();

		try {
			SequenceRegion sequence =
					mAssembly.getSequence(mGenome, mDisplayRegion, RepeatMaskType.N);

			int start = mDisplayRegion.getStart();
			int w;
			
			//System.err.println(w + " " + space.getLayoutProperties().getPlotSize().getW() + " " + mDisplayRegion.getLength());
			
			for (char c : sequence.getSequence().toArray())	{
				switch (c) {
				case 'a':
					//fullHeight = false;
				case 'A':
					g2.setColor(Dna.BASE_A_COLOR);
					break;
				case 'c':
					//fullHeight = false;
				case 'C':
					g2.setColor(Dna.BASE_C_COLOR);
					break;
				case 'g':
					//fullHeight = false;
				case 'G':
					g2.setColor(Dna.BASE_G_COLOR);
					break;
				case 't':
					//fullHeight = false;
				case 'T':
					g2.setColor(Dna.BASE_T_COLOR);
					break;
				default:
					g2.setColor(Dna.BASE_N_COLOR);
				}
				
				x1 = axes.toPlotX1(start);
				w = axes.toPlotX1(start + 1) - x1;
				
				g2.fillRect(x1, y, w, h);
				
				++start;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
