/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.htsview.tracks.dna;

import java.awt.Color;
import java.io.IOException;

import org.abh.common.bioinformatics.ext.ucsc.CytobandsService;
import org.abh.common.bioinformatics.genomic.ChromosomeSizesService;
import org.abh.common.bioinformatics.genomic.GenomicRegion;
import org.abh.sequencing.tracks.AnnotationPlotTrack;
import org.abh.sequencing.tracks.TitleProperties;
import org.abh.sequencing.tracks.TrackSubFigure;
import org.graphplot.figure.Axes2D;

// TODO: Auto-generated Javadoc
/**
 * The Class CytobandsPlotTrack.
 */
public class CytobandsPlotTrack extends AnnotationPlotTrack {
	
	/**
	 * Instantiates a new cytobands plot track.
	 */
	public CytobandsPlotTrack() {
		super("Cytobands");
	}
	
	/* (non-Javadoc)
	 * @see org.abh.sequencing.tracks.Track#getFillColor()
	 */
	@Override
	public Color getFillColor() {
		return Color.GRAY;
	}

	/* (non-Javadoc)
	 * @see org.abh.sequencing.tracks.Track#createGraph(java.lang.String, org.abh.sequencing.tracks.TitleProperties)
	 */
	@Override
	public TrackSubFigure createGraph(String genome,
			TitleProperties titlePosition) throws IOException {
		
		//
		// Display some genes
		//
		
		
		mSubFigure = CytobandsSubFigure.create("Cytobands " + genome, 
				ChromosomeSizesService.getInstance().getSizes(genome), 
				CytobandsService.getInstance().getCytobands(genome), 
				titlePosition);

		switch(titlePosition.getPosition()) {
		case RIGHT:
		case COMPACT_RIGHT:
			mSubFigure.getCurrentAxes().setMargins(SMALL_MARGIN, 
					MARGINS.getLeft(), 
					SMALL_MARGIN, 
					HUGE_MARGIN);
			break;
		default:
			mSubFigure.getCurrentAxes().setMargins(MARGINS);
			break;
		}
		
		Axes2D.disableAllFeatures(mSubFigure.getCurrentAxes());
		
		return mSubFigure;
	}
	
	/* (non-Javadoc)
	 * @see org.abh.sequencing.tracks.Track#updateGraph(org.abh.common.bioinformatics.genome.GenomicRegion, int, int, int, int)
	 */
	@Override
	public TrackSubFigure updateGraph(GenomicRegion displayRegion, 
			int resolution,
			int width,
			int height,
			int margin) throws IOException {
		
		//mPlot.setForwardCanvasEventsEnabled(false);
		
		mSubFigure.update(displayRegion, resolution, width, height, margin);

		//mPlot.setForwardCanvasEventsEnabled(true);
		
		return mSubFigure;
	}
}
