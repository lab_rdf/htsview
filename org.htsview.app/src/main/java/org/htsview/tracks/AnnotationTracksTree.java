/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.htsview.tracks;

import java.io.IOException;

import org.abh.common.Resources;
import org.abh.common.bioinformatics.conservation.ConservationAssembly;
import org.abh.common.bioinformatics.ext.ucsc.Bed;
import org.abh.common.bioinformatics.ext.ucsc.TrackDisplayMode;
import org.abh.common.bioinformatics.genomic.GenomeAssembly;
import org.abh.common.io.PathUtils;
import org.abh.common.tree.TreeNode;
import org.abh.common.tree.TreeRootNode;
import org.abh.common.ui.tree.ModernTree;
import org.abh.sequencing.tracks.Track;
import org.abh.sequencing.tracks.ext.ucsc.AnnotationBedPlotTrack;
import org.abh.sequencing.tracks.ext.ucsc.BedPlotTrack;
import org.abh.sequencing.tracks.measurement.RangePlotTrack;
import org.abh.sequencing.tracks.measurement.RulerPlotTrack;
import org.abh.sequencing.tracks.measurement.ScalePlotTrack;
import org.htsview.tracks.conservation.Conservation46WayGraphPlotTrack;
import org.htsview.tracks.conservation.Conservation46WayPlotTrack;
import org.htsview.tracks.dna.CytobandsPlotTrack;
import org.htsview.tracks.dna.DnaBasesPlotTrack;
import org.htsview.tracks.dna.DnaColorPlotTrack;
import org.htsview.tracks.dna.DnaRepeatMaskPlotTrack;
import org.htsview.tracks.genes.RefSeqGenesPlotTrack;
import org.htsview.tracks.mouse.MouseConservationPlotTrack;

// TODO: Auto-generated Javadoc
/**
 * The Class AnnotationTracksTree.
 */
public class AnnotationTracksTree extends ModernTree<Track> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new annotation tracks tree.
	 *
	 * @param genomeAssembly the genome assembly
	 * @param conservationAssembly the conservation assembly
	 * @param mouseConservationAssembly the mouse conservation assembly
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public AnnotationTracksTree(GenomeAssembly genomeAssembly,
			ConservationAssembly conservationAssembly,
			ConservationAssembly mouseConservationAssembly) throws IOException {
		Track track;
		TreeNode<Track> node;

		TreeRootNode<Track> root = new TreeRootNode<Track>();

		TreeNode<Track> humanNode = new TreeNode<Track>("Genomic");

		TreeNode<Track> genesNode = new TreeNode<Track>("Gene Annotation");

		//track = new EnsemblGenesPlotTrack();
		//node = new TreeNode<Track>(track.getName(), track);
		//genesNode.addChild(node);

		track = new RefSeqGenesPlotTrack();
		node = new TreeNode<Track>(track.getName(), track);
		genesNode.addChild(node);
		
		//track = new UcscGenesPlotTrack();
		//node = new TreeNode<Track>(track.getName(), track);
		//genesNode.addChild(node);

		track = new VegaGenesPlotTrack();
		node = new TreeNode<Track>(track.getName(), track);
		genesNode.addChild(node);

		humanNode.addChild(genesNode);

		TreeNode<Track> dnaNode = new TreeNode<Track>("DNA Annotation");

		track = new DnaBasesPlotTrack(genomeAssembly);
		node = new TreeNode<Track>(track.getName(), track);
		dnaNode.addChild(node);

		track = new DnaColorPlotTrack(genomeAssembly);
		node = new TreeNode<Track>(track.getName(), track);
		dnaNode.addChild(node);

		track = new DnaRepeatMaskPlotTrack(genomeAssembly);
		node = new TreeNode<Track>(track.getName(), track);
		dnaNode.addChild(node);

		humanNode.addChild(dnaNode);

		TreeNode<Track> conservationNode = new TreeNode<Track>("Conservation");

		track = new Conservation46WayPlotTrack(conservationAssembly);
		node = new TreeNode<Track>(track.getName(), track);
		conservationNode.addChild(node);

		track = new Conservation46WayGraphPlotTrack(conservationAssembly);
		node = new TreeNode<Track>(track.getName(), track);
		conservationNode.addChild(node);

		track = new MouseConservationPlotTrack(mouseConservationAssembly);
		node = new TreeNode<Track>(track.getName(), track);
		conservationNode.addChild(node);

		humanNode.addChild(conservationNode);
		
		
		
		
		

		TreeNode<Track> otherNode = new TreeNode<Track>("Other");
		
		track = new CytobandsPlotTrack();
		node = new TreeNode<Track>(track.getName(), track);
		otherNode.addChild(node);
		
		track = new AnnotationBedPlotTrack(Bed.parseTracks("DDS_IgLocusMap", 
				Resources.getResGzipReader("res/tracks/DDS_IgLocusMap.bed.gz")).get(0));
		node = new TreeNode<Track>(track.getName(), track);
		otherNode.addChild(node);
		
		humanNode.addChild(otherNode);
		
		root.addChild(humanNode);
		
		
		humanNode = new TreeNode<Track>("Human");
		
		TreeNode<Track> microarrayNode = new TreeNode<Track>("Microarray");

		track = new BedPlotTrack(PathUtils.getPath("res/tracks/HG-U133_Plus_2.probes.hg19.bed.gz"), TrackDisplayMode.FULL);
		node = new TreeNode<Track>(track.getName(), track);
		microarrayNode.addChild(node);
		
		humanNode.addChild(microarrayNode);
		
		root.addChild(humanNode);
		
		//
		// Mouse
		//
		
		//TreeNode<Track> mouseNode = new TreeNode<Track>("Mouse");

		//genesNode = new TreeNode<Track>("Gene Annotation");

		//track = new MouseRefSeqGenesPlotTrack(geneMap);
		//node = new TreeNode<Track>(track.getName(), track);
		//mouseNode.addChild(node);

		//mouseNode.addChild(genesNode);

		//track = new MouseCytobandsPlotTrack();
		//node = new TreeNode<Track>(track.getName(), track);
		//mouseNode.addChild(node);
		
		//root.addChild(mouseNode);
		
		//
		// Measure
		//
		
		TreeNode<Track> measureNode = new TreeNode<Track>("Measurement");

		track = new RangePlotTrack();
		node = new TreeNode<Track>(track.getName(), track);
		measureNode.addChild(node);

		track = new RulerPlotTrack();
		node = new TreeNode<Track>(track.getName(), track);
		measureNode.addChild(node);

		track = new ScalePlotTrack();
		node = new TreeNode<Track>(track.getName(), track);
		measureNode.addChild(node);
		
		root.addChild(measureNode);

		setRoot(root);
	}


}
