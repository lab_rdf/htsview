/**
 * Copyright 2017 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.htsview.tracks.view;

import java.awt.Color;
import java.io.IOException;
import java.nio.file.Path;

import org.abh.common.io.FileUtils;
import org.abh.common.json.Json;
import org.abh.common.tree.TreeNode;
import org.abh.common.ui.tree.ModernTree;
import org.abh.common.ui.window.ModernWindow;
import org.abh.sequencing.tracks.Track;
import org.abh.sequencing.tracks.TrackTreeNode;
import org.abh.sequencing.tracks.loaders.SampleLoaderService;
import org.abh.sequencing.tracks.view.TrackJsonParser;

// TODO: Auto-generated Javadoc
/**
 * The Class SampleFSJsonParser.
 */
public class SampleFSJsonParser extends TrackJsonParser {

	/* (non-Javadoc)
	 * @see org.abh.sequencing.tracks.view.TrackJsonParser#parse(org.abh.common.ui.window.ModernWindow, java.lang.String, int, org.abh.common.ui.tree.ModernTree, org.abh.common.json.Json, org.abh.common.tree.TreeNode)
	 */
	@Override
	public boolean parse(ModernWindow window,
			final String name,
			int id,
			ModernTree<Track> annotationTree,
			final Json trackJson,
			TreeNode<Track> rootNode) throws IOException {
		Path metaFile = getFile(trackJson);

		boolean validTrack = false;
		
		if (FileUtils.exists(metaFile)) {

			Track track = SampleLoaderService.getInstance().openSample(window, 
					metaFile,
					rootNode);

			Color lineColor = null;

			if (trackJson.containsKey("line-color")) {
				lineColor = trackJson.getAsColor("line-color");
			} else if (trackJson.containsKey("color")) {
				lineColor = trackJson.getAsColor("color");
			} else {
				lineColor = Color.GRAY;
			}
			
			track.setLineColor(lineColor);

			Color fillColor = null;

			if (trackJson.containsKey("fill-color")) {
				fillColor = trackJson.getAsColor("fill-color");
			} else {
				fillColor = Color.LIGHT_GRAY;
			}
			
			track.setFillColor(fillColor);

			if (trackJson.containsKey("auto-y")) {
				track.setAutoY(trackJson.getAsBool("auto-y"));
			}

			if (trackJson.containsKey("common-y")) {
				track.setCommonY(trackJson.getAsBool("common-y"));
			}

			if (trackJson.containsKey("normalize-y")) {
				track.setNormalizeY(trackJson.getAsBool("normalize-y"));
			}

			if (trackJson.containsKey("y-max")) {
				track.setYMax(trackJson.getAsInt("y-max"));
			}

			
			//TrackTreeNode child = new TrackTreeNode(track);
			//rootNode.addChild(child);

			validTrack = true;
		}
		
		return validTrack;
	}
	
	/* (non-Javadoc)
	 * @see org.abh.common.NameProperty#getName()
	 */
	@Override
	public String getName() {
		return "SampleFS";
	}
	
	/* (non-Javadoc)
	 * @see org.abh.sequencing.tracks.view.TrackJsonParser#getType()
	 */
	@Override
	public String getType() {
		return "sample-fs";
	}
}
