/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.htsview.tracks.conservation;

import java.io.IOException;

import org.abh.common.bioinformatics.conservation.ConservationAssembly;
import org.abh.common.bioinformatics.genomic.GenomicRegion;
import org.abh.sequencing.tracks.TitleProperties;
import org.abh.sequencing.tracks.TrackSubFigure;
import org.graphplot.figure.Axes2D;

// TODO: Auto-generated Javadoc
/**
 * The Class Conservation46WayGraphPlotTrack.
 */
public class Conservation46WayGraphPlotTrack extends ConservationPlotTrack {
	
	/** The title. */
	public static String TITLE = "46-way Conservation Graph";
	
	/**
	 * Instantiates a new conservation 46 way graph plot track.
	 *
	 * @param conservationAssembly the conservation assembly
	 */
	public Conservation46WayGraphPlotTrack(ConservationAssembly conservationAssembly) {
		super(TITLE, conservationAssembly);
	}

	/* (non-Javadoc)
	 * @see org.abh.sequencing.tracks.Track#createGraph(java.lang.String, org.abh.sequencing.tracks.TitleProperties)
	 */
	@Override
	public TrackSubFigure createGraph(String genome,
			TitleProperties titlePosition) throws IOException {
		
		//
		// Display some genes
		//
		
		mSubFigure = Conservation46WayGraphPlotCanvas.create(mConservationAssembly,
				titlePosition);
			
		switch(titlePosition.getPosition()) {
		case RIGHT:
		case COMPACT_RIGHT:
			int right = rightTitleWidth(getName());
			mSubFigure.getCurrentAxes().setMargins(SMALL_MARGIN, 
					MARGINS.getLeft(), 
					SMALL_MARGIN, 
					right);
			break;
		default:
			mSubFigure.getCurrentAxes().setMargins(MARGINS);
		}
		
		Axes2D.disableAllFeatures(mSubFigure.getCurrentAxes());
		
		return mSubFigure;
	}

	/* (non-Javadoc)
	 * @see org.abh.sequencing.tracks.Track#updateGraph(org.abh.common.bioinformatics.genome.GenomicRegion, int, int, int, int)
	 */
	@Override
	public TrackSubFigure updateGraph(GenomicRegion displayRegion, 
			int resolution,
			int width,
			int height,
			int margin) throws IOException {
		mSubFigure.update(displayRegion, resolution, width, height, margin);
		
		return mSubFigure;
	}
}
