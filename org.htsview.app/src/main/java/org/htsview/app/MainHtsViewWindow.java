/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.htsview.app;

import java.awt.Color;
import java.awt.FontFormatException;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.UnsupportedLookAndFeelException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.abh.common.bioinformatics.ext.ucsc.BedGraph;
import org.abh.common.bioinformatics.ext.ucsc.UCSCTrack;
import org.abh.common.bioinformatics.file.BioPathUtils;
import org.abh.common.bioinformatics.genomic.ChromosomeSizesService;
import org.abh.common.bioinformatics.genomic.GenomeAssembly;
import org.abh.common.bioinformatics.genomic.GenomicRegion;
import org.abh.common.bioinformatics.genomic.GenomicRegionModel;
import org.abh.common.bioinformatics.ui.GenomeModel;
import org.abh.common.bioinformatics.ui.external.samtools.BamGuiFileFilter;
import org.abh.common.bioinformatics.ui.external.ucsc.BedGraphGuiFileFilter;
import org.abh.common.bioinformatics.ui.external.ucsc.BedGuiFileFilter;
import org.abh.common.bioinformatics.ui.filters.GFFGuiFileFilter;
import org.abh.common.bioinformatics.ui.filters.SegGuiFileFilter;
import org.abh.common.collections.CollectionUtils;
import org.abh.common.event.ChangeEvent;
import org.abh.common.event.ChangeListener;
import org.abh.common.io.FileUtils;
import org.abh.common.io.PathUtils;
import org.abh.common.io.Temp;
import org.abh.common.json.Json;
import org.abh.common.math.matrix.AnnotatableMatrix;
import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.settings.SettingsService;
import org.abh.common.tree.TreeNode;
import org.abh.common.tree.TreeNodeEventListener;
import org.abh.common.ui.ModernComponent;
import org.abh.common.ui.UI;
import org.abh.common.ui.UIService;
import org.abh.common.ui.contentpane.CloseableHTab;
import org.abh.common.ui.contentpane.ModernHContentPane;
import org.abh.common.ui.contentpane.SizableContentPane;
import org.abh.common.ui.dialog.DialogEvent;
import org.abh.common.ui.dialog.DialogEventListener;
import org.abh.common.ui.dialog.MessageDialogType;
import org.abh.common.ui.dialog.ModernDialogStatus;
import org.abh.common.ui.dialog.ModernMessageDialog;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.event.ModernClickListener;
import org.abh.common.ui.font.FontRibbonSection;
import org.abh.common.ui.graphics.icons.QuickOpenVectorIcon;
import org.abh.common.ui.graphics.icons.QuickSaveVectorIcon;
import org.abh.common.ui.help.ModernAboutDialog;
import org.abh.common.ui.io.FileDialog;
import org.abh.common.ui.io.JpgGuiFileFilter;
import org.abh.common.ui.io.OpenRibbonPanel;
import org.abh.common.ui.io.PdfGuiFileFilter;
import org.abh.common.ui.io.PngGuiFileFilter;
import org.abh.common.ui.io.RecentFilesService;
import org.abh.common.ui.io.SaveAsRibbonPanel;
import org.abh.common.ui.io.SvgGuiFileFilter;
import org.abh.common.ui.panel.CardPanel;
import org.abh.common.ui.ribbon.QuickAccessButton;
import org.abh.common.ui.ribbon.Ribbon;
import org.abh.common.ui.ribbon.RibbonLargeButton;
import org.abh.common.ui.ribbon.RibbonMenuItem;
import org.abh.common.ui.scrollpane.ModernScrollPane;
import org.abh.common.ui.scrollpane.ScrollBarLocation;
import org.abh.common.ui.widget.ModernClickWidget;
import org.abh.common.ui.widget.ModernWidget;
import org.abh.common.ui.widget.tooltip.ModernToolTip;
import org.abh.common.ui.window.ModernRibbonWindow;
import org.abh.common.ui.zoom.ModernStatusZoomSlider;
import org.abh.common.ui.zoom.ZoomModel;
import org.abh.common.ui.zoom.ZoomRibbonSection;
import org.abh.sequencing.ngs.BctGuiFileFilter;
import org.abh.sequencing.ngs.Brt2GuiFileFilter;
import org.abh.sequencing.ngs.Brt3GuiFileFilter;
import org.abh.sequencing.ngs.BvtGuiFileFilter;
import org.abh.sequencing.tracks.AxisLimitsModel;
import org.abh.sequencing.tracks.HeightModel;
import org.abh.sequencing.tracks.LayoutRibbonSection;
import org.abh.sequencing.tracks.MarginModel;
import org.abh.sequencing.tracks.MarginRibbonSection;
import org.abh.sequencing.tracks.ResolutionModel;
import org.abh.sequencing.tracks.ResolutionService;
import org.abh.sequencing.tracks.ScaleRibbonSection;
import org.abh.sequencing.tracks.SizeRibbonSection;
import org.abh.sequencing.tracks.TitlePositionModel;
import org.abh.sequencing.tracks.Track;
import org.abh.sequencing.tracks.TrackTree;
import org.abh.sequencing.tracks.TracksFigure;
import org.abh.sequencing.tracks.WidthModel;
import org.abh.sequencing.tracks.abi.ABIGuiFileFilter;
import org.abh.sequencing.tracks.loaders.SampleLoaderService;
import org.abh.sequencing.tracks.locations.LocationsPanel;
import org.abh.sequencing.tracks.sample.SamplePlotTrack;
import org.abh.sequencing.tracks.view.TrackView;
import org.apache.batik.transcoder.TranscoderException;
import org.graphplot.Image;
import org.graphplot.ModernPlotCanvas;
import org.graphplot.figure.Graph2dStyleModel;
import org.htsview.app.Import.EncodeWorker;
import org.htsview.modules.dist.ReadDistDialog;
import org.htsview.modules.dist.ReadDistTask;
import org.htsview.modules.heatmap.HeatMapDialog;
import org.htsview.modules.heatmap.HeatMapTask;
import org.htsview.tracks.AnnotationTracksTree;
import org.htsview.tracks.HTSTracksPanel;
import org.matcalc.MainMatCalc;
import org.matcalc.MainMatCalcWindow;
import org.matcalc.bio.BioModuleLoader;
import org.matcalc.figure.graph2d.Graph2dStyleRibbonSection;
import org.rdf.bedgraph.MainBedGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import edu.columbia.rdf.edb.Sample;
import edu.columbia.rdf.edb.ui.network.ServerException;


// TODO: Auto-generated Javadoc
/**
 * Display BedGraph and other track data.
 * 
 * @author Antony Holmes Holmes
 *
 */
public class MainHtsViewWindow extends ModernRibbonWindow implements ModernClickListener {

	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/** The Constant LOG. */
	private final static Logger LOG = 
			LoggerFactory.getLogger(MainHtsViewWindow.class);

	/**
	 * The constant MAX_PLOT_POINTS.
	 */
	private static final int MAX_PLOT_POINTS = 100000;

	//private static final String MAX_POINTS_MESSAGE = 
	//		"Please adjust the display to show fewer than " + TextUtils.commaFormat(MAX_PLOT_POINTS) + " data points.";

	/**
	 * The m open panel.
	 */
	private OpenRibbonPanel mOpenPanel = new OpenRibbonPanel();

	/**
	 * The m save as panel.
	 */
	private SaveAsRibbonPanel mSaveAsPanel = new SaveAsRibbonPanel();

	/**
	 * The m zoom model.
	 */
	private ZoomModel mZoomModel = new ReadsZoomModel();

	/**
	 * The m content pane.
	 */
	private ModernHContentPane mContentPane = 
			new ModernHContentPane();

	//private BedGraphsModel mBedGraphsModel = new BedGraphsModel();

	/**
	 * The m genomic model.
	 */
	private GenomicRegionModel mGenomicModel = new GenomicRegionModel();

	/**
	 * The m y axis limit model.
	 */
	private AxisLimitsModel mYAxisLimitModel = new AxisLimitsModel();



	/**
	 * The m genome model.
	 */
	private GenomeModel mGenomeModel = new GenomeModel();

	/**
	 * The m resolution model.
	 */
	private ResolutionModel mResolutionModel = new ResolutionModel();

	/**
	 * The m peak style model.
	 */
	private Graph2dStyleModel mPeakStyleModel = new ReadsStyleModel();

	/** The m width model. */
	private WidthModel mWidthModel = new WidthModel();

	/** The m height model. */
	private HeightModel mHeightModel = new HeightModel();

	/** The m margin model. */
	private MarginModel mMarginModel = new MarginModel();

	/** The m track list. */
	private TrackTree mTrackList = new TrackTree();

	/**
	 * The m canvas.
	 */
	private TracksFigure mTracksFigure;

	/**
	 * The m tracks panel.
	 */
	private HTSTracksPanel mTracksPanel;

	/**
	 * The m title position model.
	 */
	private TitlePositionModel mTitlePositionModel =
			new TitlePositionModel();

	/**
	 * The m annotation tree.
	 */
	private AnnotationTracksTree mAnnotationTree;

	/**
	 * The m locations panel.
	 */
	private LocationsPanel mLocationsPanel;

	/** The m font section. */
	private FontRibbonSection mFontSection;

	private boolean mSuggestSave = false;

	private Path mViewFile;

	//private AxesControlPanel mFormatPane;

	/**
	 * The constant PREVIOUS_XML_VIEW_FILE.
	 */
	//private static final Path PREVIOUS_XML_VIEW_FILE = 
	//		PathUtils.getPath("previous.readsx");

	/**
	 * The constant PREVIOUS_JSON_VIEW_FILE.
	 */
	//private static final Path PREVIOUS_JSON_VIEW_FILE = 
	//		PathUtils.getPath("previous.readsj");

	//private static final GenomicRegion DEFAULT_REGION =
	//		new GenomicRegion(Chromosome.CHR1, 1000000, 1100000);

	/**
	 * The class ResolutionEvents.
	 */
	private class ResolutionEvents implements ChangeListener {

		/* (non-Javadoc)
		 * @see org.abh.lib.event.ChangeListener#changed(org.abh.lib.event.ChangeEvent)
		 */
		@Override
		public void changed(ChangeEvent e) {
			setResolution();
		}
	}

	/**
	 * The class PeakStyleEvents.
	 */
	private class PeakStyleEvents implements ChangeListener {

		/* (non-Javadoc)
		 * @see org.abh.lib.event.ChangeListener#changed(org.abh.lib.event.ChangeEvent)
		 */
		@Override
		public void changed(ChangeEvent e) {
			try {
				style();
			} catch (IOException | TransformerException | ParserConfigurationException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * The class GenomeEvents.
	 */
	private class GenomeEvents implements ChangeListener {

		/* (non-Javadoc)
		 * @see org.abh.lib.event.ChangeListener#changed(org.abh.lib.event.ChangeEvent)
		 */
		@Override
		public void changed(ChangeEvent e) {
			try {
				recreatePlots(); //updatePlots();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * The class GenomicEvents.
	 */
	private class GenomicEvents implements ChangeListener {

		/* (non-Javadoc)
		 * @see org.abh.lib.event.ChangeListener#changed(org.abh.lib.event.ChangeEvent)
		 */
		@Override
		public void changed(ChangeEvent e) {
			try {
				updateResolution();
			} catch (IOException | ParseException | TransformerException | ParserConfigurationException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * The class AxisEvents.
	 */
	private class AxisEvents implements ChangeListener {

		/* (non-Javadoc)
		 * @see org.abh.lib.event.ChangeListener#changed(org.abh.lib.event.ChangeEvent)
		 */
		@Override
		public void changed(ChangeEvent e) {
			try {
				axis();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * The class TitlePositionEvents.
	 */
	private class TitlePositionEvents implements ChangeListener {

		/* (non-Javadoc)
		 * @see org.abh.lib.event.ChangeListener#changed(org.abh.lib.event.ChangeEvent)
		 */
		@Override
		public void changed(ChangeEvent e) {
			try {
				recreatePlots();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * The Class WidthEvents.
	 */
	private class WidthEvents implements ChangeListener {

		/* (non-Javadoc)
		 * @see org.abh.common.event.ChangeListener#changed(org.abh.common.event.ChangeEvent)
		 */
		@Override
		public void changed(ChangeEvent e) {
			try {
				updatePlots();
			} catch (TransformerException | ParserConfigurationException | IOException e1) {
				e1.printStackTrace();
			}
		}

	}

	/**
	 * The class TreeEvents.
	 */
	private class TrackEvents implements TreeNodeEventListener {

		/* (non-Javadoc)
		 * @see org.abh.lib.tree.TreeNodeEventListener#nodeChanged(org.abh.lib.event.ChangeEvent)
		 */
		@Override
		public void nodeChanged(ChangeEvent e) {
			try {
				recreatePlots();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		/* (non-Javadoc)
		 * @see org.abh.lib.tree.TreeNodeEventListener#nodeUpdated(org.abh.lib.event.ChangeEvent)
		 */
		@Override
		public void nodeUpdated(ChangeEvent e) {
			try {
				updatePlots();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * So we can respond to the user deciding whether to overwrite a file.
	 * 
	 * @author Antony Holmes Holmes
	 *
	 */
	private class ExportCallBack implements DialogEventListener {

		/**
		 * The m file.
		 */
		protected Path mFile;

		/**
		 * The m pwd.
		 */
		protected Path mPwd;

		/**
		 * Instantiates a new export call back.
		 *
		 * @param pwd the pwd
		 * @param file the file
		 */
		public ExportCallBack(Path pwd, Path file) {
			mFile = file;
			mPwd = pwd;
		}

		/* (non-Javadoc)
		 * @see org.abh.common.ui.ui.dialog.DialogEventListener#statusChanged(org.abh.common.ui.ui.dialog.DialogEvent)
		 */
		@Override
		public void statusChanged(DialogEvent e) {
			if (e.getStatus() == ModernDialogStatus.OK) {
				try {	
					save(mFile);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} else {
				try {
					export(mPwd);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	/**
	 * The Class CloseSaveCallBack.
	 */
	private class CloseSaveCallBack extends ExportCallBack {

		/**
		 * Instantiates a new close save call back.
		 *
		 * @param pwd the pwd
		 * @param file the file
		 */
		public CloseSaveCallBack(Path pwd, Path file) {
			super(file, pwd);
		}

		/* (non-Javadoc)
		 * @see org.htsview.app.MainHtsViewWindow.ExportCallBack#statusChanged(org.abh.common.ui.dialog.DialogEvent)
		 */
		@Override
		public void statusChanged(DialogEvent e) {
			if (e.getStatus() == ModernDialogStatus.OK) {
				try {	
					save(mFile);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} else {
				try {
					closeSave(mPwd);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}



	/**
	 * The class SaveAction.
	 */
	private class SaveAction extends AbstractAction {

		/**
		 * The constant serialVersionUID.
		 */
		private static final long serialVersionUID = 1L;

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				export();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (TranscoderException e1) {
				e1.printStackTrace();
			} catch (TransformerException e1) {
				e1.printStackTrace();
			} catch (ParserConfigurationException e1) {
				e1.printStackTrace();
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * The class OpenAction.
	 */
	private class OpenAction extends AbstractAction {

		/**
		 * The constant serialVersionUID.
		 */
		private static final long serialVersionUID = 1L;

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				browseForFiles();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
	}

	/**
	 * Instantiates a new main reads window.
	 *
	 * @param genome the genome
	 * @param tree the tree
	 * @param samples the samples
	 * @throws SAXException the SAX exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	public MainHtsViewWindow(String genome,
			AnnotationTracksTree tree,
			Collection<Sample> samples) throws SAXException, IOException, ParserConfigurationException {
		super(new HTSViewInfo());

		mGenomeModel.set(genome);

		mAnnotationTree = tree;

		init(samples);
	}

	/**
	 * Inits the.
	 *
	 * @param samples the samples
	 * @throws SAXException the SAX exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	private void init(Collection<Sample> samples) throws SAXException, IOException, ParserConfigurationException {
		JComponent content = (JComponent)getContentPane();

		content.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_MASK), "open");
		content.getActionMap().put("open", new OpenAction());

		content.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK), "save");
		content.getActionMap().put("save", new SaveAction());

		mTracksFigure = new TracksFigure(mGenomicModel, 
				ChromosomeSizesService.getInstance().getSizes(GenomeAssembly.HG19));

		mLocationsPanel = new LocationsPanel(this,
				mGenomeModel,
				mGenomicModel);

		mTracksPanel = new HTSTracksPanel(this, mAnnotationTree, mTrackList);

		/*
		mFormatPanel = new FormatPanel(this, 
				mTracksFigure.getcu, 
				mTracksFigure.getGenesProperties());
		 */

		// We'll default to looking at chr1 for the region of the plot
		//GenomicRegion region = new GenomicRegion(Chromosome.CHR1, 
		//		100000,
		//		101000);

		// Goto the position of an interesting gene as a default
		//GenomicRegion region = 
		//		mGeneMap.get(mGenomeModel.get()).findMainVariant("BCL6");

		GenomicRegion region = GenomicRegion.parse(SettingsService.getInstance().getAsString("edb.reads.default-location"));

		mGenomicModel.set(region);


		createRibbon();

		createUi();


		mGenomeModel.addChangeListener(new GenomeEvents());
		mGenomicModel.addChangeListener(new GenomicEvents());
		//mSamplesModel.addSelectionListener(new SampleEvents());
		mResolutionModel.addChangeListener(new ResolutionEvents());
		mYAxisLimitModel.addChangeListener(new AxisEvents());
		mWidthModel.addChangeListener(new WidthEvents());
		mHeightModel.addChangeListener(new WidthEvents());
		mMarginModel.addChangeListener(new WidthEvents());
		mPeakStyleModel.addChangeListener(new PeakStyleEvents());
		//mTracksModel.addChangeListener(new TrackEvents());
		mTitlePositionModel.addChangeListener(new TitlePositionEvents());

		// Monitor how the tracks are being updated
		mTracksPanel.getTree().addTreeNodeListener(new TrackEvents());

		setSize(1400, 800);

		UI.centerWindowToScreen(this);

		// Set the initial look



		// load what the user was last looking at
		//if (FileUtils.exists(PREVIOUS_XML_VIEW_FILE) && 
		//		SettingsService.getInstance().getAsBool("edb.reads.auto-load-previous-view")) {
		//	loadXmlView(PREVIOUS_XML_VIEW_FILE);
		//}

		// Load samples in the non-interactive mode so users aren't annoyed
		// with screens asking them for options
		loadSamples(samples, false);
	}

	/**
	 * Load samples.
	 *
	 * @param samples the samples
	 * @param interactive the interactive
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void loadSamples(Collection<Sample> samples, boolean interactive) throws IOException {
		mTracksPanel.loadSamples(samples, interactive);
	}

	/**
	 * Creates the ribbon.
	 */
	public final void createRibbon() {
		//RibbongetRibbonMenu() getRibbonMenu() = new RibbongetRibbonMenu()(0);
		RibbonMenuItem menuItem;

		menuItem = new RibbonMenuItem(UI.MENU_NEW_WINDOW);
		getRibbonMenu().addTabbedMenuItem(menuItem);

		menuItem = new RibbonMenuItem(UI.MENU_OPEN);
		getRibbonMenu().addTabbedMenuItem(menuItem, mOpenPanel);

		//menuItem = new RibbonMenuItem("Open Track");
		//getRibbonMenu().addTabbedMenuItem(menuItem);

		//menuItem = new RibbonMenuItem("Open BRT Track");
		//getRibbonMenu().addTabbedMenuItem(menuItem);

		//menuItem = new RibbonMenuItem("Import");
		//getRibbonMenu().addTabbedMenuItem(menuItem);

		//menuItem = new RibbonMenuItem("Import Multi-Res");
		//getRibbonMenu().addTabbedMenuItem(menuItem);

		menuItem = new RibbonMenuItem(UI.MENU_SAVE_AS);
		getRibbonMenu().addTabbedMenuItem(menuItem, mSaveAsPanel);

		//menuItem = new RibbonMenuItem("Export To BedGraph");
		//getRibbonMenu().addTabbedMenuItem(menuItem);

		getRibbonMenu().addDefaultItems(getAppInfo());

		getRibbonMenu().addClickListener(this);

		getRibbonMenu().setDefaultIndex(1);


		ModernClickWidget button;

		//Ribbon2 ribbon = new Ribbon2();
		getRibbon().setHelpButtonEnabled(getAppInfo());

		button = new QuickAccessButton(UIService.getInstance().loadIcon(QuickOpenVectorIcon.class, 
				Ribbon.BAR_BACKGROUND,
				16));
		button.setClickMessage(UI.MENU_OPEN);
		button.setToolTip(new ModernToolTip("Open", "Open peak files."));
		button.addClickListener(this);
		addQuickAccessButton(button);

		button = new QuickAccessButton(UIService.getInstance().loadIcon(QuickSaveVectorIcon.class, 16));
		button.setClickMessage(UI.MENU_SAVE);
		button.setToolTip(new ModernToolTip("Save", "Save the current image."));
		button.addClickListener(this);
		addQuickAccessButton(button);

		//RibbonSection toolbarContainer;

		//// home
		//RibbonToolbar toolbar = new RibbonToolbar("Home");

		//toolbarSection = new ClipboardRibbonSection(ribbon);
		//toolbar.add(toolbarSection);

		getRibbon().getHomeToolbar().add(new GenomeRibbonSection(getRibbon(), mGenomeModel));

		getRibbon().getHomeToolbar().add(new HTSGenomicRibbonSection(getRibbon(), mGenomicModel, mGenomeModel));
		getRibbon().getHomeToolbar().add(new ScaleRibbonSection(getRibbon(), "Y Scale", mYAxisLimitModel));
		//getRibbon().getHomeToolbar().add(new HeightRibbonSection(mHeightModel));
		getRibbon().getHomeToolbar().add(new ResolutionRibbonSection(getRibbon(), mResolutionModel));
		getRibbon().getHomeToolbar().add(new Graph2dStyleRibbonSection(getRibbon(), mPeakStyleModel));

		mFontSection = new FontRibbonSection(this);
		mFontSection.setup(mTracksFigure.getFont(), Color.BLACK);
		getRibbon().getToolbar("Layout").add(mFontSection);
		getRibbon().getToolbar("Layout").add(new LayoutRibbonSection(getRibbon(), mTitlePositionModel));
		getRibbon().getToolbar("Layout").add(new MarginRibbonSection(getRibbon(), mMarginModel));
		getRibbon().getToolbar("Layout").add(new SizeRibbonSection(getRibbon(), mWidthModel, mHeightModel));

		//
		// View
		//



		button = new RibbonLargeButton("Locations", 
				UIService.getInstance().loadIcon("locations", 24),
				"Locations List",
				"Show a list of locations");
		button.addClickListener(this);
		getRibbon().getToolbar("View").getSection("Locations").add(button);




		mFontSection.addChangeListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent e) {
				mTracksFigure.setFont(mFontSection.getUserFont(), mFontSection.getFontColor());
			}});


		getRibbon().getToolbar("View").add(new ZoomRibbonSection(this, mZoomModel));


		//
		// Tools
		//

		button = new RibbonLargeButton("Read Distribution", 
				UIService.getInstance().loadIcon("read_dist", 32),
				UIService.getInstance().loadIcon("read_dist", 24),
				"Read Distribution",
				"Read distribution plot");
		button.addClickListener(this);
		getRibbon().getToolbar("Tools").getSection("Tools").add(button);

		button = new RibbonLargeButton("Heat Map", 
				UIService.getInstance().loadIcon("tss_heatmap", 32),
				"TSS Heat Map",
				"Create TSS Heat Map");
		button.addClickListener(this);
		getRibbon().getToolbar("Tools").getSection("Tools").add(button);

		button = new RibbonLargeButton("Reads", 
				UIService.getInstance().loadIcon("reads", 24),
				"Reads",
				"Create table of reads from selected samples");
		button.addClickListener(this);
		getRibbon().getToolbar("Tools").getSection("Tools").add(button);

		//toolbarSection = new PlotSizeRibbonSection(mCanvas.getSubPlotLayout());
		//toolbar.add(toolbarSection);

		//ZoomRibbonSection zoomSection = 
		//		new ZoomRibbonSection(this, zoomModel, ribbon);

		//toolbar.add(zoomSection);

		//LegendRibbonSection legendSection =
		//		new LegendRibbonSection(mCanvas.getGraphProperties().getLegend());

		//toolbar.add(legendSection);


		/*
		button = new RibbonLargeButton2("Format",
				new Raster32Icon(new FormatPlot32VectorIcon()));
		button.addClickListener(new ModernClickListener() {

			@Override
			public void clicked(ModernClickEvent e) {
				addFormatPane();
			}});

		getRibbon().getToolbar("View").getSection("Format").add(button);
		 */


		//setRibbon(ribbon, getRibbonMenu());

		getRibbon().setSelectedIndex(1);
	}

	/*
	public void setFormatPane(FormatPlotPane formatPane) {
		this.formatPane = formatPane;

		addFormatPane();
	}
	 */

	/* (non-Javadoc)
	 * @see org.abh.common.ui.ui.window.ModernWindow#createUi()
	 */
	@Override
	public final void createUi() {

		//mSamplesPanel = new SamplesTreePanel(mReadAssembly, 
		//		mSamplesModel);

		//ImageCanvas imageCanvas = new ImageCanvas(mCanvas);

		//mFormatPane = new AxesControlPanel(this, mCanvas);

		//ZoomCanvas zoomCanvas = new ZoomCanvas(mTracksFigure);
		mTracksFigure.setZoomModel(mZoomModel);

		//ContainerCanvas cc = new FrameCanvas(zoomCanvas);

		//ZoomCanvas zoomCanvas = new ZoomCanvas(canvas);



		//BackgroundCanvas backgroundCanvas = new BackgroundCanvas(zoomCanvas);

		ModernScrollPane scrollPane = new ModernScrollPane(mTracksFigure);
		scrollPane.setScrollBarLocation(ScrollBarLocation.FLOATING);
		//scrollPane.setHorizontalScrollBarPolicy(ScrollBarPolicy.NEVER);
		//ModernPanel panel = new ModernPanel(scrollPane);

		//ModernPanel panel = new ModernPanel(zoomCanvas);

		//panel.setBorder(ModernPanel.BORDER);

		mContentPane.getModel().setCenterTab(new ModernComponent(new CardPanel(new ModernComponent(scrollPane, ModernWidget.BORDER)), ModernWidget.DOUBLE_BORDER));

		//mPanel = new Graph2dPanel(this,
		//		mCanvas,
		//		history,
		//		zoomModel,
		//		mContentPane.getModel());

		//setFormatPane(mPanel);

		setBody(mContentPane);

		getStatusBar().addRight(new ModernStatusZoomSlider(mZoomModel));

		addTracksPane();

		//addLocationsPane();
	}

	/**
	 * Adds the group pane to the layout if it is not already showing.
	 */
	/*
	private void addSamplesPanel() {
		if (mContentPane.getModel().contains("Samples")) {
			return;
		}


		mContentPane.getModel().addLeft(new SizableContentPane("Samples", 
				new CloseableHTab("Samples", mSamplesPanel, mContentPane.getModel()), 250, 200, 500));
	}
	 */

	/*
	private void addFormatPane() {
		if (mContentPane.getModel().containsTab("Format Plot")) {
			return;
		}

		mContentPane.getModel().addTab(new SizableContentPane("Format Plot", 
				new CloseableHTab("Format Plot", mFormatPanel, mContentPane.getModel()), 300, 200, 500));
	}
	 */

	private void addTracksPane() {
		if (mContentPane.getModel().getLeftTabs().containsTab("Tracks")) {
			return;
		}

		SizableContentPane sizePane = new SizableContentPane("Tracks", 
				mTracksPanel, 
				250, 
				100, 
				500);

		//CollapseHTab htab = new CollapseHTab(sizePane, mTracksPanel);

		//sizePane.setComponent(htab);

		mContentPane.getModel().addLeftTab(sizePane);
	}

	/**
	 * Adds the locations pane.
	 */
	private void addLocationsPane() {
		if (mContentPane.getModel().getRightTabs().containsTab("Locations")) {
			return;
		}

		//CollapseHTab htab = new CollapseHTab(sizePane, mTracksPanel);

		//sizePane.setComponent(htab);

		mContentPane.getModel().getRightTabs().addTab(new SizableContentPane("Locations", 
				new CloseableHTab("Locations", mLocationsPanel, mContentPane), 250, 100, 500));
	}

	/*
	private void addFormatPane() {
		if (mContentPane.getModel().getRightTabs().containsTab("Format")) {
			return;
		}

		mContentPane.getModel().getRightTabs().addTab(new SizableContentPane("Format", 
				new CloseableHTab2("Format", mFormatPane, mContentPane), 300, 200, 500));
	}
	 */

	/* (non-Javadoc)
	 * @see org.abh.common.ui.ui.event.ModernClickListener#clicked(org.abh.common.ui.ui.event.ModernClickEvent)
	 */
	@Override
	public final void clicked(ModernClickEvent e) {
		if (e.getMessage().equals(UI.MENU_NEW_WINDOW)) {
			MainHtsViewWindow window;
			try {
				window = new MainHtsViewWindow(mGenomeModel.get(),
						mAnnotationTree,
						null);

				window.setVisible(true);
			} catch (SAXException | IOException | ParserConfigurationException e1) {
				e1.printStackTrace();
			}


		} else if (e.getMessage().equals(UI.MENU_OPEN) ||
				e.getMessage().equals(UI.MENU_BROWSE)) {
			try {
				browseForFiles();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else if (e.getMessage().equals(OpenRibbonPanel.FILE_SELECTED)) {
			try {
				openFiles(mOpenPanel.getSelectedFile());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else if (e.getMessage().equals(OpenRibbonPanel.DIRECTORY_SELECTED)) {
			try {
				browseForFiles(mOpenPanel.getSelectedDirectory());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else if (e.getMessage().equals(UI.MENU_SAVE)) {
			try {
				export();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (TranscoderException e1) {
				e1.printStackTrace();
			} catch (TransformerException e1) {
				e1.printStackTrace();
			} catch (ParserConfigurationException e1) {
				e1.printStackTrace();
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		} else if (e.getMessage().equals(SaveAsRibbonPanel.DIRECTORY_SELECTED)) {
			try {
				export(mSaveAsPanel.getSelectedDirectory());
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (TranscoderException e1) {
				e1.printStackTrace();
			} catch (TransformerException e1) {
				e1.printStackTrace();
			} catch (ParserConfigurationException e1) {
				e1.printStackTrace();
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		} else if (e.getMessage().equals("Import")) {
			try {
				importSam();
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		} else if (e.getMessage().equals("Locations")) {
			addLocationsPane();
		} else if (e.getMessage().equals("Read Distribution")) {
			try {
				readDist();
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		} else if (e.getMessage().equals("Heat Map")) {
			try {
				heatMap();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (SAXException e1) {
				e1.printStackTrace();
			} catch (ParserConfigurationException e1) {
				e1.printStackTrace();
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		} else if (e.getMessage().equals("Reads")) {
			reads();
		} else if (e.getMessage().equals(UI.MENU_ABOUT)) {
			ModernAboutDialog.show(this, getAppInfo());
		} else if (e.getMessage().equals(UI.MENU_EXIT)) {
			close();
		} else {

		}
	}

	/**
	 * Import sam.
	 *
	 * @throws ParseException the parse exception
	 */
	private void importSam() throws ParseException {
		ImportDialog dialog = new ImportDialog(this);

		dialog.setVisible(true);

		if (dialog.getStatus() == ModernDialogStatus.CANCEL) {
			return;
		}

		if (dialog.getSamFile() == null) {
			ModernMessageDialog.createWarningDialog(this, 
					"You must select a file.");

			return;
		}

		if (dialog.getDir() == null) {
			ModernMessageDialog.createWarningDialog(this, 
					"You must select a directory.");

			return;
		}

		EncodeWorker task = new Import.EncodeWorker(this, 
				dialog.getSamFile(), 
				dialog.getDir(),
				dialog.getName(),
				dialog.getOrganism(),
				dialog.getGenome(), 
				dialog.getReadLength(), 
				dialog.getResolutions());

		try {
			task.doInBackground();
		} catch (Exception e) {
			e.printStackTrace();
		} // task.execute();

	}

	/**
	 * Force the whole plot to be recreated. This is more expensive than
	 * updating.
	 *
	 * @throws Exception the exception
	 */
	private void recreatePlots() throws Exception {

		mTracksFigure.setTracks(mTracksPanel.getTree(),
				mGenomeModel.get(),
				mPeakStyleModel.get(),
				mTitlePositionModel.get());



		// Save the view for reloading.
		updatePlots();
	}

	/**
	 * Update plots.
	 *
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void updatePlots() throws TransformerException, ParserConfigurationException, IOException {
		mTracksFigure.refresh(mGenomicModel.get(),
				mResolutionModel.get(),
				mWidthModel.get(),
				mHeightModel.get(),
				mMarginModel.get());

		mSuggestSave = true;

		// Save the view for reloading.

		//saveXmlView(PREVIOUS_XML_VIEW_FILE);
		//saveJsonView(PREVIOUS_JSON_VIEW_FILE);
	}


	/**
	 * Sets the resolution.
	 */
	private void setResolution() {
		try {
			updateResolution();
		} catch (IOException | ParseException | TransformerException | ParserConfigurationException e) {
			createResolutionErrorDialog();

			e.printStackTrace();
		}
	}

	/**
	 * Creates the resolution error dialog.
	 */
	private void createResolutionErrorDialog() {
		ModernMessageDialog.createWarningDialog(this, 
				"The samples cannot be displayed at a " + ResolutionService.getHumanReadable(mResolutionModel.get()) + 
				" resolution.",
				"The resolution will be adjusted back to the previous setting of " + ResolutionService.getHumanReadable(mResolutionModel.getPrevious()) + ".");

		mResolutionModel.set(mResolutionModel.getPrevious());
	}

	/**
	 * Update resolution.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParseException the parse exception
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	private void updateResolution() throws IOException, ParseException, TransformerException, ParserConfigurationException {

		GenomicRegion displayRegion = mGenomicModel.get();

		int resolution = mResolutionModel.get();

		int bins = displayRegion.getLength() / resolution;

		//System.err.println("update res " + bins + " " + resolution);

		// Auto scale the resolution to match the region of interest,
		// Users can of course change it
		if (bins == 0) {
			mResolutionModel.set(mResolutionModel.get() / 10);
		} else if (bins > MAX_PLOT_POINTS) {
			// lets decrease the resolution until we find something that works

			mResolutionModel.set(mResolutionModel.get() * 10);

			//createInformationDialog(MAX_POINTS_MESSAGE);
		} else {
			updatePlots();
		}
	}


	/**
	 * Update axis events.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParseException the parse exception
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	private void axis() throws IOException, ParseException, TransformerException, ParserConfigurationException {
		for (TreeNode<Track> node : mTracksPanel.getTree()) {
			Track track = node.getValue();

			if (track != null) {
				track.setYProperties(mYAxisLimitModel);
			}
		}

		updatePlots();
	}

	/**
	 * Style.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	private void style() throws IOException, TransformerException, ParserConfigurationException {
		for (TreeNode<Track> node : mTracksPanel.getTree()) {
			Track track = node.getValue();

			if (track != null) {
				track.setStyle(mPeakStyleModel.get());
			}
		}

		updatePlots();
	}

	/**
	 * Heat map.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SAXException the SAX exception
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws ParseException the parse exception
	 */
	private void heatMap() throws IOException, SAXException, ParserConfigurationException, ParseException {
		List<SamplePlotTrack> tracks = new ArrayList<SamplePlotTrack>();

		for (Track track : mTracksPanel.getSelectedTracks()) {
			if (track instanceof SamplePlotTrack) {
				tracks.add((SamplePlotTrack)track);
			}
		}

		if (tracks.size() == 0) {
			ModernMessageDialog.createWarningDialog(this, 
					"You must select at least one sample.");
			return;
		}

		HeatMapDialog dialog = new HeatMapDialog(this,
				mGenomeModel,
				tracks);

		dialog.setVisible(true);

		if (dialog.getStatus() == ModernDialogStatus.CANCEL) {
			return;
		}

		HeatMapTask task = new HeatMapTask(this,
				tracks,
				dialog.getInput(),
				dialog.getRegions(),
				dialog.getPadding(),
				dialog.getBinSize(),
				dialog.getSortType(),
				mGenomeModel);

		//task.execute();
		task.doInBackground();
		task.done();
	}

	/**
	 * Read dist.
	 *
	 * @throws ParseException the parse exception
	 */
	private void readDist() throws ParseException {

		List<SamplePlotTrack> sampleTracks = new ArrayList<SamplePlotTrack>();

		for (Track track : mTracksPanel.getSelectedTracks()) {
			if (track instanceof SamplePlotTrack) {
				sampleTracks.add((SamplePlotTrack)track);
			}
		}

		if (sampleTracks.size() == 0) {
			ModernMessageDialog.createWarningDialog(this, 
					"You must select a sample.");
			return;
		}

		ReadDistDialog dialog = new ReadDistDialog(this,
				mGenomeModel,
				sampleTracks);

		dialog.setVisible(true);

		if (dialog.getStatus() == ModernDialogStatus.CANCEL) {
			return;
		}

		ModernDialogStatus status = ModernMessageDialog.createOkCancelInfoDialog(this, 
				"Generating distribution plots can take several minutes.");

		if (status == ModernDialogStatus.CANCEL) {
			return;
		}

		ReadDistTask task = new ReadDistTask(this,
				dialog.getPlotName(),
				sampleTracks, 
				dialog.getRegions(), 
				dialog.getPadding(),
				dialog.getBinSize(),
				dialog.getAverage());

		task.doInBackground(); //execute();

		/*
		HeatMapTask task = new HeatMapTask(this,
				dialog.getSample(),
				dialog.getInput(),
				dialog.getRegions(),
				dialog.getPadding(),
				dialog.getBinSize(),
				dialog.getSortType(),
				dialog.getShouldPlot(),
				mGeneMap,
				mGenomeModel);

		task.execute();
		 */
	}

	/**
	 * Extract reads in the given region.
	 */
	private void reads() {

		List<SamplePlotTrack> sampleTracks = new ArrayList<SamplePlotTrack>();

		for (Track track : mTracksPanel.getSelectedTracks()) {
			if (track instanceof SamplePlotTrack) {
				sampleTracks.add((SamplePlotTrack)track);
			}
		}

		if (sampleTracks.size() == 0) {
			ModernMessageDialog.createWarningDialog(this, 
					"You must select some samples");

			return;
		}

		List<Integer> starts = new ArrayList<Integer>();

		GenomicRegion region = mGenomicModel.get();

		int l = -1;

		for (SamplePlotTrack track : sampleTracks) {
			try {
				starts.addAll(track.getAssembly().getStarts(track.getSample(), region, -1));

				if (track.getAssembly().getReadLength(track.getSample()) > 0) {
					l = track.getAssembly().getReadLength(track.getSample());
				}

				System.err.println(starts);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		AnnotationMatrix m = AnnotatableMatrix.createAnnotatableMatrix(starts.size(), 3);

		m.setName("Reads");
		m.setColumnNames("chr", "start", "end");

		for (int i = 0; i < starts.size(); ++i) {
			int start = starts.get(i);

			m.set(i, 0, region.getChr());
			m.set(i, 1, start);
			m.set(i, 2, start + l - 1);
		}

		try {
			MainMatCalcWindow window = 
					MainMatCalc.main(getAppInfo(), new BioModuleLoader());

			window.openMatrix(m);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (FontFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Browse for files.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParseException the parse exception
	 * @throws SAXException the SAX exception
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	public void browseForFiles() throws IOException, ParseException, SAXException, ParserConfigurationException {
		browseForFiles(RecentFilesService.getInstance().getPwd());
	}

	/**
	 * Browse for files.
	 *
	 * @param pwd the pwd
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParseException the parse exception
	 * @throws SAXException the SAX exception
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	public void browseForFiles(Path pwd) throws IOException, ParseException, SAXException, ParserConfigurationException {
		openFiles(FileDialog.open(this).filter(new ReadsAllSupportedGuiFileFilter(),
				new BamGuiFileFilter(),
				new BctGuiFileFilter(),
				new Brt2GuiFileFilter(),
				new Brt3GuiFileFilter(),
				new BvtGuiFileFilter(),
				new BedGuiFileFilter(),
				new BedGraphGuiFileFilter(),
				new GFFGuiFileFilter(),
				new SegGuiFileFilter(),
				new ABIGuiFileFilter(),
				new ReadsViewGuiFileFilter()).getFiles(pwd));
	}

	/**
	 * Open files.
	 *
	 * @param file the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParseException the parse exception
	 * @throws SAXException the SAX exception
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	public void openFiles(Path file) throws IOException, ParseException, SAXException, ParserConfigurationException {
		openFiles(CollectionUtils.asList(file));
	}

	/**
	 * Open files.
	 *
	 * @param files the files
	 * @throws ParseException the parse exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SAXException the SAX exception
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	public void openFiles(List<Path> files) throws ParseException, IOException, SAXException, ParserConfigurationException {
		List<Path> otherFiles = new ArrayList<Path>();

		for (Path file : files) {
			if (PathUtils.ext().type(ReadsJsonViewGuiFileFilter.EXT).test(file)) {
				loadJsonView(file);
			} else if (PathUtils.ext().type(ReadsXmlViewGuiFileFilter.EXT).test(file)) {
				//loadXmlView(file);
			} else {
				otherFiles.add(file);
			}

			RecentFilesService.getInstance().add(file);
		}

		if (otherFiles.size() > 0) {
			SampleLoaderService.getInstance().openFiles(this, files, mTrackList.getRoot());
		}
	}



	/**
	 * Load xml view.
	 *
	 * @param jsonFile the json file
	 * @throws ParseException the parse exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	/*
	public void loadXmlView(Path file) throws SAXException, IOException, ParserConfigurationException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = factory.newSAXParser();

		TracksXmlHandler handler = 
				new TracksXmlHandler(mAssembly, mAnnotationTree, mTracksPanel);

		saxParser.parse(file.toFile(), handler);

		// load the tracks
		TreeRootNode<Track> tracks = handler.getTracks();

		mTracksPanel.setTracks(tracks);

		mGenomicModel.set(handler.getRegion());
	}
	 */

	/**
	 * Load json view.
	 *
	 * @param jsonFile the json file
	 * @throws ParseException the parse exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void loadJsonView(Path jsonFile) throws ParseException, IOException {
		TrackView.loadJsonView(this,
				jsonFile,
				mTracksPanel,
				mAnnotationTree, 
				mWidthModel, 
				mMarginModel, 
				mGenomicModel,
				mTitlePositionModel);

		mViewFile = jsonFile;
		mSuggestSave = false;
	}

	/**
	 * Export to bed graph.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParseException the parse exception
	 */
	private void exportToBedGraph() throws IOException {
		Path tmp = Temp.createTempFile("bedgraph");

		BedGraph.write(getBedGraphs(), tmp);

		MainBedGraph.main(tmp);
	}

	/**
	 * Gets the bed graphs.
	 *
	 * @return the bed graphs
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParseException the parse exception
	 */
	private List<UCSCTrack> getBedGraphs() throws IOException {
		List<UCSCTrack> bedgraphs = new ArrayList<UCSCTrack>();

		for (TreeNode<Track> node : mTracksPanel.getTree()) {
			Track track = node.getValue();

			UCSCTrack bedGraph = track.getBedGraph(mGenomicModel.get(), 
					mResolutionModel.get());

			if (bedGraph == null) {
				bedgraphs.add(bedGraph);
			}
		}

		return bedgraphs;
	}

	/**
	 * Export.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TranscoderException the transcoder exception
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws ParseException the parse exception
	 */
	private void export() throws IOException, TranscoderException, TransformerException, ParserConfigurationException, ParseException {
		export(RecentFilesService.getInstance().getPwd());
	}

	/**
	 * Export.
	 *
	 * @param pwd the pwd
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TranscoderException the transcoder exception
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws ParseException the parse exception
	 */
	private void export(Path pwd) throws IOException, TranscoderException, TransformerException, ParserConfigurationException, ParseException {
		Path file = FileDialog.save(this).filter( 
				new SvgGuiFileFilter(),
				new PngGuiFileFilter(),
				new PdfGuiFileFilter(),
				new JpgGuiFileFilter(),
				new BedGraphGuiFileFilter(),
				new ReadsJsonViewGuiFileFilter()).getFile(pwd);

		save(pwd, file);

		/*
		if (FileUtils.exists(file)) {
			ModernDialogStatus status = 
					ModernMessageDialog.createFileReplaceDialog(this, file);

			if (status == ModernDialogStatus.CANCEL) {
				export(pwd);

				return;
			}
		}

		if (Io.getFileExtension(file).equals("readsx")) {
			saveView(file);
		} else if (Io.getFileExtension(file).equals("bed")) {
			Bed.writeBedGraphAsBed(mTracksModel.getBedGraphs(mGenomicModel.get(), mResolutionModel.get()), file);

			//ModernMessageDialog.createFileSavedDialog(this, getAppInfo().getName(), file);
		} else if (Io.getFileExtension(file).equals("bedgraph")) {
			BedGraph.write(mTracksModel.getBedGraphs(mGenomicModel.get(), mResolutionModel.get()), file);

			//ModernMessageDialog.createFileSavedDialog(this, getAppInfo().getName(), file);
		} else {
			Image.write(this, getCanvas(), file);
		}

		RecentFilesService.getInstance().add(file);

		createFileSavedDialog(file);
		 */
	}

	private void saveView() throws IOException, TranscoderException, TransformerException, ParserConfigurationException, ParseException {
		saveView(RecentFilesService.getInstance().getPwd());
	}

	public void saveView(Path pwd) throws IOException, TranscoderException, TransformerException, ParserConfigurationException, ParseException {
		Path file = FileDialog.save(this).filter(new ReadsJsonViewGuiFileFilter()).getFile(pwd);

		save(pwd, file);
	}

	/**
	 * Save.
	 *
	 * @param pwd the pwd
	 * @param file the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TranscoderException the transcoder exception
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws ParseException the parse exception
	 */
	private void save(Path pwd, Path file) throws IOException, TranscoderException, TransformerException, ParserConfigurationException, ParseException {
		save(pwd, file, new ExportCallBack(pwd, file));
	}

	/**
	 * Save.
	 *
	 * @param pwd the pwd
	 * @param file the file
	 * @param l the l
	 * @return 
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TranscoderException the transcoder exception
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws ParseException the parse exception
	 */
	private boolean save(Path pwd, Path file, DialogEventListener l) throws IOException, TranscoderException, TransformerException, ParserConfigurationException, ParseException {
		if (file == null) {
			return false;
		}

		if (FileUtils.exists(file)) {
			ModernMessageDialog.createFileReplaceDialog(this, file, l);
			
			return false;
		} else {
			return save(file);
		}
	}

	/**
	 * Save.
	 *
	 * @param file the file
	 * @return 
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TranscoderException the transcoder exception
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws ParseException the parse exception
	 */
	private boolean save(Path file) throws IOException, TranscoderException, TransformerException, ParserConfigurationException {
		if (file == null) {
			return false;
		}

		if (PathUtils.ext().type(ReadsXmlViewGuiFileFilter.EXT).test(file)) {
			saveXmlView(file);
		} else if (PathUtils.ext().type(ReadsJsonViewGuiFileFilter.EXT).test(file)) {
			saveJsonView(file);
		} else if (BioPathUtils.ext().bedgraph().test(file)) {
			exportToBedGraph();
		} else {
			Image.write(getCanvas(), file);
		}

		//RecentFilesService.getInstance().add(file);

		ModernMessageDialog.createFileSavedDialog(this, file);
		
		return true;
	}

	/**
	 * Save xml view.
	 *
	 * @param file the file
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 */
	private void saveXmlView(Path file) throws TransformerException, ParserConfigurationException {
		mTracksPanel.saveXmlView(file, 
				mGenomicModel.get(),
				mWidthModel.get(),
				mMarginModel.get());
		
		RecentFilesService.getInstance().add(file);
	}

	/**
	 * Save json view.
	 *
	 * @param file the file
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void saveJsonView(Path file) throws IOException {
		TrackView.saveJsonView(file,
				mTrackList,
				mGenomicModel.get(),
				mTitlePositionModel.get(),
				mWidthModel.get(),
				mMarginModel.get());
		
		// Once a view is saved, stop asking the user until they
		// update something.
		mSuggestSave = false;
		
		RecentFilesService.getInstance().add(file);
	}

	/**
	 * Gets the canvas.
	 *
	 * @return the canvas
	 */
	public ModernPlotCanvas getCanvas() {
		return mTracksFigure;
	}

	/* (non-Javadoc)
	 * @see org.abh.common.ui.ui.window.ModernWindow#close()
	 */
	@Override
	public void close() {
		if (mSuggestSave) {
			ModernDialogStatus status = ModernMessageDialog.createDialog(this,
					MessageDialogType.WARNING_OK_CANCEL,
					"The current view has not been saved.", 
					"Would you like to save it?");

			if (status == ModernDialogStatus.OK) {
				if (mViewFile != null) {
					// If the view exists, save it.
					try {
						save(mViewFile);
					} catch (IOException | TranscoderException | TransformerException | ParserConfigurationException e) {
						e.printStackTrace();
					}
				} else {
					// If the view does not exist, prompt user for location.
					try {
						saveView();
					} catch (IOException | TranscoderException | TransformerException | ParserConfigurationException
							| ParseException e) {
						e.printStackTrace();
					}
				}
			} else {
				// If the user chooses not to save, prompt one last time that
				// the view is unsaved if they exit.
				
				status = ModernMessageDialog.createDialog(this, 
						MessageDialogType.WARNING_OK_CANCEL,
						"The current view has not been saved.", 
						"Are you sure you want to exit?");

				if (status == ModernDialogStatus.CANCEL) {
					return;
				}
			}
		}

		/*
		if (status == ModernDialogStatus.OK) {
			try {
				if (mViewFile != null) {
					closeSave(RecentFilesService.getInstance().getPwd(), 
							mViewFile);
				} else {
					closeSave();
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (TranscoderException e) {
				e.printStackTrace();
			}
		}
		 */

		Temp.deleteTempFiles();

		super.close();
	}

	/**
	 * Close save.
	 *
	 * @param pwd the pwd
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TranscoderException the transcoder exception
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws ParseException the parse exception
	 */
	private void closeSave(Path pwd) throws IOException, TranscoderException, TransformerException, ParserConfigurationException, ParseException {
		closeSave(pwd, null);
	}

	/**
	 * Close save.
	 *
	 * @param pwd the pwd
	 * @param suggested the suggested
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws TranscoderException the transcoder exception
	 * @throws TransformerException the transformer exception
	 * @throws ParserConfigurationException the parser configuration exception
	 * @throws ParseException the parse exception
	 */
	private void closeSave(Path pwd, Path suggested) throws IOException, TranscoderException, TransformerException, ParserConfigurationException, ParseException {
		Path file;

		if (suggested != null) {
			file = FileDialog
					.save(this)
					.filter(new ReadsJsonViewGuiFileFilter())
					.suggested(suggested)
					.getFile(pwd);
		} else {
			file = FileDialog
					.save(this)
					.filter(new ReadsJsonViewGuiFileFilter())
					.getFile(pwd);
		}

		save(pwd, file, new CloseSaveCallBack(pwd, file));
	}

	/* (non-Javadoc)
	 * @see org.abh.common.ui.window.ModernWindow#restart()
	 */
	@Override
	public void restart() {
		if (ModernMessageDialog.createRestartDialog(this) == ModernDialogStatus.OK) {
			close();

			try {
				MainHtsView.main((String[])null);
			} catch (ServerException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (FontFormatException e) {
				e.printStackTrace();
			} catch (UnsupportedLookAndFeelException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Gets the file.
	 *
	 * @param json the json
	 * @return the file
	 */
	private static Path getFile(Json json) {
		String path = json.getAsString("file");

		if (path != null) {
			return PathUtils.getPath(path);
		}

		path = json.getAsString("meta-file");

		if (path != null) {
			return PathUtils.getPath(path);
		}

		return null;
	}
}
