/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.htsview.modules.heatmap;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.SwingWorker;

import org.abh.common.Properties;
import org.abh.common.bioinformatics.genomic.Gene;
import org.abh.common.bioinformatics.genomic.GenesService;
import org.abh.common.bioinformatics.genomic.GenomicRegion;
import org.abh.common.bioinformatics.ui.GenomeModel;
import org.abh.common.collections.CollectionUtils;
import org.abh.common.collections.DefaultTreeMap;
import org.abh.common.collections.TreeSetCreator;
import org.abh.common.io.FileUtils;
import org.abh.common.io.Temp;
import org.abh.common.math.matrix.AnnotatableMatrix;
import org.abh.common.math.matrix.AnnotationMatrix;
import org.abh.common.ui.graphics.colormap.ColorMap;
import org.abh.common.ui.window.ModernRibbonWindow;
import org.abh.sequencing.tracks.SampleAssembly;
import org.abh.sequencing.tracks.sample.SamplePlotTrack;
import org.graphplot.AspectRatio;
import org.graphplot.figure.heatmap.ColorNormalizationType;
import org.matcalc.MainMatCalc;
import org.matcalc.MainMatCalcWindow;
import org.matcalc.bio.BioModuleLoader;
import org.matcalc.toolbox.plot.heatmap.HeatMapProperties;

import edu.columbia.rdf.edb.Sample;

// TODO: Auto-generated Javadoc
/**
 * The Class HeatMapTask.
 */
public class HeatMapTask extends SwingWorker<Void, Void> {

	/** The m matrices. */
	private List<AnnotationMatrix> mMatrices;
	
	/** The m regions. */
	private List<HeatMapIdLocation> mRegions;
	
	/** The m padding. */
	private int mPadding;
	
	/** The m window. */
	private int mWindow;
	
	/** The m heat map sort. */
	private HeatMapSort mHeatMapSort;
	
	/** The m input. */
	private Sample mInput;
	
	/** The m parent. */
	private ModernRibbonWindow mParent;

	/** The m genome model. */
	private GenomeModel mGenomeModel;

	/** The m samples. */
	//private SampleAssembly mAssembly;
	private List<SamplePlotTrack> mSamples;

	/**
	 * Instantiates a new heat map task.
	 *
	 * @param parent the parent
	 * @param samples the samples
	 * @param input the input
	 * @param regions the regions
	 * @param padding the padding
	 * @param window the window
	 * @param heatMapSort the heat map sort
	 * @param genomeModel the genome model
	 */
	public HeatMapTask(ModernRibbonWindow parent,
			List<SamplePlotTrack> samples,
			Sample input, 
			List<HeatMapIdLocation> regions, 
			int padding,
			int window,
			HeatMapSort heatMapSort,
			GenomeModel genomeModel) {
		mParent = parent;
		mSamples = samples;
		mInput = input;
		mRegions = regions;
		mPadding = padding;
		mWindow = window;
		mHeatMapSort = heatMapSort;
		mGenomeModel = genomeModel;
	}

	/* (non-Javadoc)
	 * @see javax.swing.SwingWorker#doInBackground()
	 */
	@Override
	public Void doInBackground() {
		try {
			mMatrices = createHeatMapMatrices();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	/* (non-Javadoc)
	 * @see javax.swing.SwingWorker#done()
	 */
	@Override
	public void done() {
		Properties properties = new HeatMapProperties();

		properties.setProperty("plot.colormap", ColorMap.createWhiteRedMap());
		properties.setProperty("plot.aspect-ratio", new AspectRatio(0.1));
		properties.setProperty("plot.show-grid-color", false);
		properties.setProperty("plot.show-outline-color", false);
		properties.setProperty("plot.show-row-labels", false);
		properties.setProperty("plot.color.standardization", 
				ColorNormalizationType.ZSCORE_MATRIX);

		properties.setProperty("plot.color.intensity", 2);

		try {
			MainMatCalcWindow window = 
					MainMatCalc.main(mParent.getAppInfo(), new BioModuleLoader());

			window.openMatrices(mMatrices);

			window.runModule("Heat Map", "--plot");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creates the heat map matrices.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	private List<AnnotationMatrix> createHeatMapMatrices() throws Exception {
		//int window = 100;
		int bins = 2 * (mPadding / mWindow) + 1;

		List<AnnotationMatrix> ret = new ArrayList<AnnotationMatrix>();

		for (SamplePlotTrack track : mSamples) {
			Sample sample = track.getSample();
			
			AnnotationMatrix m = 
					AnnotatableMatrix.createNumericalMatrix(mRegions.size(), bins);
			
			m.setName(sample.getName());
			
			for (int i = 0; i < bins; ++i) {
				m.setColumnName(i, Integer.toString((i - bins / 2) * mWindow));
			}
			
			

			Path temp = Temp.generateTempFile("txt");

			BufferedWriter writer = FileUtils.newBufferedWriter(temp);

			try {
				switch (mHeatMapSort) {
				case TSS_DISTANCE:
					sortTssDist(track, bins, m);
					break;
				case INTENSITY:
					sortIntensity(track, bins, m);
					break;
				default:
					sortNone(track, bins, m);
					break;
				}
			} finally {
				writer.close();
			}
			
			ret.add(m);
		}

		return ret;
	}

	/**
	 * Sort none.
	 *
	 * @param sample the sample
	 * @param bins the bins
	 * @param m the m
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParseException the parse exception
	 */
	private void sortNone(SamplePlotTrack sample, int bins, AnnotationMatrix m) throws IOException, ParseException {	
		//for (HeatMapIdLocation region : mRegions) {
		for (int i = 0; i < mRegions.size(); ++i) {
			HeatMapIdLocation region = mRegions.get(i);
			
			if (region.getRegion() == null) {
				continue;
			}

			GenomicRegion ext = GenomicRegion.extend(region.getRegion(), 
					mPadding, 
					mPadding);

			// +- 2kb

			List<Double> counts = getCounts(sample, ext, mWindow);

			//writer.write(gene.getRefSeq());
			//writer.write(TextUtils.TAB_DELIMITER);
			//writer.write(gene.getEntrez());
			//writer.write(TextUtils.TAB_DELIMITER);
			//writer.write(gene.getSymbol());

			m.setRowAnnotation("Id", i, region.getId());
			m.setRowAnnotation("Location", i, region.getRegion().toString());
			
			for (int j = 0; j < counts.size(); ++j) {
				System.err.println("heat " + i + " " + j + " " + counts.get(j) + " " + region.getRegion().toString());
				m.set(i, j, counts.get(j));
			}
		}
	}

	/**
	 * Sort tss dist.
	 *
	 * @param sample the sample
	 * @param bins the bins
	 * @param m the m
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParseException the parse exception
	 */
	private void sortTssDist(SamplePlotTrack sample, int bins, AnnotationMatrix m) throws IOException, ParseException {	
		Map<Integer, Set<String>> tssMap =
				new TreeMap<Integer, Set<String>>();

		Set<String> used = new HashSet<String>();

		for (int i = 0; i < mRegions.size(); ++i) {
			HeatMapIdLocation region = mRegions.get(i);
			
			if (region.getRegion() == null) {
				continue;
			}

			List<Gene> closestGenes = 
					GenesService.getInstance().getGenes(mGenomeModel.get(), "refseq").findClosestGenesByTss(region.getRegion());

			//System.err.println("sym " + closestGenes.get(0).getSymbol());

			int tssDistance = Gene.tssDist5p(closestGenes.get(0), region.getRegion()); //GenomicRegion.midDist(region.getRegion(), Gene.tssRegion(closestGenes.get(0)));

			//System.err.println("tss " + tssDistance + " " + closestGenes.get(0).getLocation() + " " + region.getRegion());

			if (!tssMap.containsKey(tssDistance)) {
				tssMap.put(tssDistance, new TreeSet<String>());
			}

			for (Gene gene : closestGenes) {
				if (used.contains(gene.getSymbol())) {
					continue;
				}

				tssMap.get(tssDistance).add(gene.getRefSeq());
				used.add(gene.getSymbol());
			}
		}

		int i = 0;
		
		for (int tssDist : tssMap.keySet()) {
			for (String refseq : tssMap.get(tssDist)) {
				Gene gene = GenesService.getInstance().getGenes(mGenomeModel.get(), "refseq").lookupByRefSeq(refseq);

				GenomicRegion tssRegion = Gene.tssRegion(gene);

				GenomicRegion ext = GenomicRegion.extend(tssRegion, 
						mPadding, 
						mPadding);


				// +- 2kb

				List<Double> counts = getCounts(sample, ext, mWindow);

				m.setRowAnnotation("Id", i, refseq);
				m.setRowAnnotation("Location", i, ext.getLocation());

				for (int j = 0; j < counts.size(); ++j) {
					m.set(i, j, counts.get(j));
				}

				++i;
			}
		}
	}


	/**
	 * Sort intensity.
	 *
	 * @param sample the sample
	 * @param bins the bins
	 * @param m the m
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParseException the parse exception
	 */
	private void sortIntensity(SamplePlotTrack sample, 
			int bins, 
			AnnotationMatrix m) throws IOException, ParseException {	
		Map<Double, Set<GenomicRegion>> tssMap =
				DefaultTreeMap.create(new TreeSetCreator<GenomicRegion>()); //new TreeMap<Double, Set<GenomicRegion>>();

		Map<GenomicRegion, List<Double>> countMap = 
				new HashMap<GenomicRegion, List<Double>>();

		Map<GenomicRegion, GenomicRegion> extMap = 
				new HashMap<GenomicRegion, GenomicRegion>();

		for (int i = 0; i < mRegions.size(); ++i) {
			HeatMapIdLocation region = mRegions.get(i);
			
			if (region.getRegion() == null) {
				continue;
			}


			GenomicRegion r = region.getRegion();

			//List<Gene> closestGenes = 
			//		mGeneMap.get(mGenomeModel.get()).findClosestGenesByTss(region);

			// The region is already a midpoint, so we just extend it
			GenomicRegion ext = GenomicRegion.extend(r, 
					mPadding, 
					mPadding);

			System.err.println("closest " + r.toString());

			// Find closest tss to peak and center on that
			//GenomicRegion ext = GenomicRegion.extend(Gene.tssRegion(closestGenes.get(0)), 
			//		mPadding, 
			//		mPadding);

			List<Double> counts = getCounts(sample, ext, mWindow);

			double sum = 0;

			for (double count : counts) {
				sum += count;
			}

			tssMap.get(sum).add(r);

			countMap.put(r, counts);

			extMap.put(r, ext);
		}

		int i = 0;

		for (double sum : CollectionUtils.reverse(CollectionUtils.sort(tssMap.keySet()))) {
			for (GenomicRegion region : tssMap.get(sum)) {
				GenomicRegion ext = extMap.get(region); //GenomicRegion.extend(GenomicRegion.midRegion(region), mPadding, mPadding);

				
				// +- 2kb

				List<Double> counts = countMap.get(region); //getCounts(ext, mWindow);

				
				m.setRowAnnotation("Id", i, region.getLocation());
				m.setRowAnnotation("Location", i, ext.getLocation());

				for (int j = 0; j < counts.size(); ++j) {
					System.err.println("intensity " + i + " " + j + " " + counts.get(j) + " " + region.toString());
					
					m.set(i, j, counts.get(j));
				}

				++i;
			}
		}
	}


	/**
	 * Get the counts and subtract the input if necessary.
	 *
	 * @param sample the sample
	 * @param ext the ext
	 * @param mWindow the m window
	 * @return the counts
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ParseException the parse exception
	 */
	private List<Double> getCounts(SamplePlotTrack sample,
			GenomicRegion ext, 
			int mWindow) throws IOException, ParseException {
		SampleAssembly assembly = sample.getAssembly();
		
		List<Double> counts = assembly.getNormalizedCounts(sample.getSample(),
				ext,
				mWindow);

		if (mInput != null) {
			List<Double> inputCounts = assembly.getNormalizedCounts(mInput,
					ext,
					mWindow);

			for (int i = 0; i < counts.size(); ++i) {
				counts.set(i, Math.max(0, counts.get(i) - inputCounts.get(i)));
			}
		}

		return counts;
	}
}
