/**
 * Copyright 2016 Antony Holmes
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.htsview.modules.dist;

import java.awt.Dimension;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.Box;

import org.abh.common.bioinformatics.ext.ucsc.Bed;
import org.abh.common.bioinformatics.ext.ucsc.BedGraph;
import org.abh.common.bioinformatics.ext.ucsc.UCSCTrack;
import org.abh.common.bioinformatics.file.BioPathUtils;
import org.abh.common.bioinformatics.genomic.Chromosome;
import org.abh.common.bioinformatics.genomic.Gene;
import org.abh.common.bioinformatics.genomic.GenesService;
import org.abh.common.bioinformatics.genomic.GenomicRegion;
import org.abh.common.bioinformatics.ui.Bioinformatics;
import org.abh.common.bioinformatics.ui.GenomeModel;
import org.abh.common.bioinformatics.ui.external.ucsc.BedGraphGuiFileFilter;
import org.abh.common.bioinformatics.ui.external.ucsc.BedGraphTableModel;
import org.abh.common.bioinformatics.ui.external.ucsc.BedGuiFileFilter;
import org.abh.common.bioinformatics.ui.external.ucsc.BedTableModel;
import org.abh.common.collections.CollectionUtils;
import org.abh.common.io.FileUtils;
import org.abh.common.math.ui.external.microsoft.AllXlsxGuiFileFilter;
import org.abh.common.math.ui.external.microsoft.XlsxGuiFileFilter;
import org.abh.common.text.TextUtils;
import org.abh.common.ui.UI;
import org.abh.common.ui.UIService;
import org.abh.common.ui.button.ModernButton;
import org.abh.common.ui.button.ModernCheckBox;
import org.abh.common.ui.dataview.ModernDataModel;
import org.abh.common.ui.dialog.ModernDialogFlatButton;
import org.abh.common.ui.dialog.ModernDialogHelpWindow;
import org.abh.common.ui.dialog.ModernDialogStatus;
import org.abh.common.ui.dialog.ModernMessageDialog;
import org.abh.common.ui.event.ModernClickEvent;
import org.abh.common.ui.graphics.icons.OpenFolderVectorIcon;
import org.abh.common.ui.io.FileDialog;
import org.abh.common.ui.io.RecentFilesService;
import org.abh.common.ui.io.TxtGuiFileFilter;
import org.abh.common.ui.list.ModernList;
import org.abh.common.ui.list.ModernListModel;
import org.abh.common.ui.panel.HBox;
import org.abh.common.ui.panel.ModernLineBorderPanel;
import org.abh.common.ui.panel.ModernPanel;
import org.abh.common.ui.panel.VBox;
import org.abh.common.ui.scrollpane.ModernScrollPane;
import org.abh.common.ui.scrollpane.ScrollBarPolicy;
import org.abh.common.ui.text.ModernAutoSizeLabel;
import org.abh.common.ui.text.ModernTextBorderPanel;
import org.abh.common.ui.text.ModernTextField;
import org.abh.common.ui.widget.ModernWidget;
import org.abh.common.ui.window.ModernWindow;
import org.abh.common.ui.window.WindowWidgetFocusEvents;
import org.abh.sequencing.tracks.sample.SamplePlotTrack;
import org.htsview.app.ResolutionComboBox;
import org.htsview.modules.heatmap.HeatMapIdLocation;
import org.htsview.modules.heatmap.RegionsPanel;


// TODO: Auto-generated Javadoc
/**
 * The Class ReadDistDialog.
 */
public class ReadDistDialog extends ModernDialogHelpWindow {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The m samples button. */
	private ModernButton mSamplesButton = 
			new ModernDialogFlatButton("Samples...");

	/** The m file button. */
	private ModernButton mFileButton = new ModernButton(UI.MENU_LOAD, 
			UIService.getInstance().loadIcon(OpenFolderVectorIcon.class, 16));

	/** The m genes button. */
	private ModernButton mGenesButton = new ModernButton("All Genes");

	/** The m text padding. */
	private ModernTextField mTextPadding =
			new ModernTextField("3000");

	/** The m samples list. */
	private ModernList<String> mSamplesList = new ModernList<String>();

	/** The m text bin. */
	private ResolutionComboBox mTextBin = new ResolutionComboBox();

	//private ModernCheckBox mCheckUseInput =
	//		new ModernCheckBox("Use input", ModernWidget.STANDARD_SIZE);

	/** The m check plot. */
	private ModernCheckBox mCheckPlot = 
			new ModernCheckBox("Create plot", true);
	
	/** The m check average. */
	private ModernCheckBox mCheckAverage = 
			new ModernCheckBox("Average count", true);

	/** The m regions panel. */
	private RegionsPanel mRegionsPanel;

	/** The m name field. */
	private ModernTextField mNameField = new ModernTextField("Samples");

	/** The m genome model. */
	private GenomeModel mGenomeModel;

	/** The m samples. */
	private List<SamplePlotTrack> mSamples;

	/** The Constant LIST_SIZE. */
	private static final Dimension LIST_SIZE = new Dimension(540, 140);

	/**
	 * Instantiates a new read dist dialog.
	 *
	 * @param parent the parent
	 * @param genomeModel the genome model
	 * @param samples the samples
	 */
	public ReadDistDialog(ModernWindow parent, 
			GenomeModel genomeModel,
			List<SamplePlotTrack> samples) {
		super(parent, "htsview.modules.read-dist.help.url");

		mGenomeModel = genomeModel;

		mRegionsPanel = new RegionsPanel(genomeModel);

		setTitle("Read Distribution");

		setup();

		createUi();

		loadSamples(samples);
	}

	/**
	 * Setup.
	 */
	private void setup() {
		addWindowListener(new WindowWidgetFocusEvents(mOkButton));

		mGenesButton.addClickListener(this);
		mFileButton.addClickListener(this);
		mSamplesButton.addClickListener(this);

		setSize(720, 680);
		
		UI.centerWindowToScreen(this);
	}

	/**
	 * Creates the ui.
	 */
	private final void createUi() {
		Box box = VBox.create();
		Box box2;

		box2 = HBox.create();
		box2.add(new ModernAutoSizeLabel("Name", 100));
		box2.add(new ModernTextBorderPanel(mNameField, ModernWidget.VERY_LARGE_SIZE));
		box.add(box2);
		
		midSectionHeader("Samples", box);

		ModernScrollPane scrollPane = new ModernScrollPane(mSamplesList);
		scrollPane.setHorizontalScrollBarPolicy(ScrollBarPolicy.NEVER);
		ModernLineBorderPanel panel = 
				new ModernLineBorderPanel(scrollPane, LIST_SIZE);
		box.add(panel);


		midSectionHeader("Plot Locations", box);

		UI.setSize(mRegionsPanel, LIST_SIZE);

		box2 = HBox.create();
		mRegionsPanel.setAlignmentY(TOP_ALIGNMENT);
		box2.add(mRegionsPanel);

		box2.add(UI.createHGap(5));

		Box box3 = VBox.create();
		box3.setAlignmentY(TOP_ALIGNMENT);
		box3.add(mFileButton);
		box3.add(ModernPanel.createVGap());
		box3.add(mGenesButton);
		box2.add(box3);
		box.add(box2);

		box.add(UI.createVGap(40));

		box2 = HBox.create();
		box2.add(new ModernAutoSizeLabel("Extend"));
		box2.add(UI.createHGap(10));
		box2.add(new ModernTextBorderPanel(mTextPadding, ModernWidget.STANDARD_SIZE));
		box2.add(ModernPanel.createHGap());
		box2.add(new ModernAutoSizeLabel("bp"));
		box2.add(UI.createHGap(40));
		box2.add(new ModernAutoSizeLabel("Bin size"));
		box2.add(UI.createHGap(10));
		box2.add(mTextBin);
		box.add(box2);
		
		box.add(UI.createVGap(10));
		box.add(mCheckAverage);
		box.add(UI.createVGap(5));
		box.add(mCheckPlot);

		setDialogCardContent(box);

		mTextBin.setSelectedIndex(2);
	}

	/* (non-Javadoc)
	 * @see org.abh.common.ui.dialog.ModernDialogTaskWindow#clicked(org.abh.common.ui.event.ModernClickEvent)
	 */
	public final void clicked(ModernClickEvent e) {
		if (e.getSource().equals(mOkButton)) {
			try {
				if (mRegionsPanel.getRegions().size() > 0) {
					setStatus(ModernDialogStatus.OK);

					close();
				} else {
					ModernMessageDialog.createWarningDialog(mParent, 
							"You must enter at least one gene or region to plot.");
				}
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		} else if (e.getSource().equals(mGenesButton)) {
			loadTss();
		} else if (e.getSource().equals(mFileButton)) {
			try {
				browseForFile();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else {
			close();
		}
	}

	/*
	private void loadSamples() throws IOException {
		ChipSeqSamplesDialog dialog = 
				new ChipSeqSamplesDialog(mParent, mAssembly, mSearchModel);

		dialog.setVisible(true);

		if (dialog.getStatus() == ModernDialogStatus.CANCEL) {
			return;
		}

		loadSamples(dialog.getSelectedSamples());
	}
	*/
	
	/**
	 * Load samples.
	 *
	 * @param samples the samples
	 */
	private void loadSamples(List<SamplePlotTrack> samples) {
		mSamples = samples;

		ModernListModel<String> model = new ModernListModel<String>();

		for (SamplePlotTrack sample : mSamples) {
			model.addValue(sample.getName());
		}

		mSamplesList.setModel(model);
	}

	/**
	 * Load tss.
	 */
	private void loadTss() {
		Set<String> genes = new HashSet<String>();

		for (String refseq : GenesService.getInstance().getGenes(mGenomeModel.get(), "refseq").getRefSeqIds()) {
			Gene gene = GenesService.getInstance().getGenes(mGenomeModel.get(), "refseq").lookupByRefSeq(refseq);

			//GenomicRegion tss = Gene.tssRegion(gene);

			genes.add(gene.getSymbol());
		}

		mRegionsPanel.setText(TextUtils.join(CollectionUtils.sort(genes), 
				TextUtils.NEW_LINE_DELIMITER));
	}

	/**
	 * Browse for file.
	 *
	 * @throws Exception the exception
	 */
	private void browseForFile() throws Exception {
		browseForFile(RecentFilesService.getInstance().getPwd());
	}

	/**
	 * Browse for file.
	 *
	 * @param workingDirectory the working directory
	 * @throws Exception the exception
	 */
	private void browseForFile(Path workingDirectory) throws Exception {
		openFile(FileDialog.openFile(getParentWindow(), 
				workingDirectory, 
				new AllXlsxGuiFileFilter(),
				new XlsxGuiFileFilter(),
				new TxtGuiFileFilter(),
				new BedGuiFileFilter(),
				new BedGraphGuiFileFilter()));
	}

	/**
	 * Open file.
	 *
	 * @param file the file
	 * @throws Exception the exception
	 */
	private void openFile(Path file) throws Exception {
		if (file == null) {
			return;
		}

		if (!FileUtils.exists(file)) {
			ModernMessageDialog.createFileDoesNotExistDialog(getParentWindow(), 
					getAppInfo().getName(), 
					file);

			return;
		}

		ModernDataModel model;

		if (BioPathUtils.ext().bed().test(file)) {
			UCSCTrack bed = Bed.parseTracks(file).get(0);

			model = new BedTableModel(bed);
		} else if (BioPathUtils.ext().bedgraph().test(file)) {
			UCSCTrack bed = BedGraph.parse(file).get(0);

			model = new BedGraphTableModel(bed);
		} else {
			model = Bioinformatics.getModel(file, true, TextUtils.emptyList(), 0, TextUtils.TAB_DELIMITER);
		}

		StringBuilder buffer = new StringBuilder();
		GenomicRegion region = null;

		for (int i = 0; i < model.getRowCount(); ++i) {
			if (GenomicRegion.isGenomicRegion(model.getValueAsString(i, 0))) {
				region = GenomicRegion.parse(model.getValueAsString(i, 0));

				GenomicRegion mid = GenomicRegion.midRegion(region);

				buffer.append(mid.toString()).append(TextUtils.NEW_LINE_DELIMITER);
			} else if (model.getValueAsString(i, 0).startsWith("chr")) {
				// three column format

				region = new GenomicRegion(Chromosome.parse(model.getValueAsString(i, 0)),
						model.getValueAsInt(i, 1),
						model.getValueAsInt(i, 2));

				GenomicRegion mid = GenomicRegion.midRegion(region);

				buffer.append(mid.toString()).append(TextUtils.NEW_LINE_DELIMITER);
			} else {
				// assume its a gene id/symbol etc.
				buffer.append(model.getValueAsString(i, 0)).append(TextUtils.NEW_LINE_DELIMITER);
			}
		}

		mRegionsPanel.setText(buffer.toString());

		RecentFilesService.getInstance().add(file);
	}

	/**
	 * Gets the regions.
	 *
	 * @return the regions
	 * @throws ParseException the parse exception
	 */
	public List<HeatMapIdLocation> getRegions() throws ParseException {
		return mRegionsPanel.getRegions();
	}

	/**
	 * Gets the padding.
	 *
	 * @return the padding
	 */
	public int getPadding() {
		return Integer.parseInt(mTextPadding.getText());
	}

	/**
	 * Gets the should plot.
	 *
	 * @return the should plot
	 */
	public boolean getShouldPlot() {
		return mCheckPlot.isSelected();
	}

	/**
	 * Gets the bin size.
	 *
	 * @return the bin size
	 */
	public int getBinSize() {
		// Min resolution is 1
		return (int)Math.pow(10, mTextBin.getSelectedIndex());
	}

	/**
	 * Gets the plot name.
	 *
	 * @return the plot name
	 */
	public String getPlotName() {
		return mNameField.getText();
	}
	
	/**
	 * Gets the average.
	 *
	 * @return the average
	 */
	public boolean getAverage() {
		return mCheckAverage.isSelected();
	}
}
