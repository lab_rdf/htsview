# README #

## Download And Installation

* Download the latest version from [Downloads](https://bitbucket.org/lab_rdf/htsview/downloads/).
* Unzip HTSView into a directory of your choice.
* HTSView does not require installation and therefore does not require administrator privileges to run.

## Running

HTSView requires Java 1.7 or above and the Java installation must have been added to the system path so it can be discovered.

* **Windows** - Double click htsview.bat.
* **macOS** - Double click htsview.command.
* **Linux** - Run htsview.sh.

Please read the [Wiki](https://bitbucket.org/lab_rdf/htsview/wiki/Home) for more detailed usage instructions.